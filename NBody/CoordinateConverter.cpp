#include "CoordinateConverter.h"

CoordinateConverter::CoordinateConverter() : _toViewFactor(0.0f), _toWorldFactor(0.0f)
{
	_viewCenter = { 0, 0 };
	_worldCenter = Vector::Zero();
}

void CoordinateConverter::Update(POINT& viewCenter, Vector& worldCenter, FPOINT maxWorldDistance)
{
	static auto factor = 0.1f;
	_viewCenter = viewCenter;
	_worldCenter = Vector::Add(Vector::Scale(_worldCenter, 1.0f - factor), Vector::Scale(worldCenter, factor));
	UpdateMaxDistance(maxWorldDistance);
}

void CoordinateConverter::UpdateMaxDistance(FPOINT maxDistance)
{
	//Set converter factor
	_toViewFactor = MIN(_viewCenter.x, _viewCenter.y) / maxDistance;
	_toWorldFactor = 1.0f / _toViewFactor;
}

FPOINT CoordinateConverter::Converter(FPOINT p1, FPOINT c1, FPOINT factor, FPOINT c2, bool invert)
{
	return c2 + (invert ? -1.0f : 1.0f)*factor*(p1 - c1);
}

FPOINT CoordinateConverter::ConverterToView(FPOINT worldPoint, FPOINT worldCenter, int viewCenter, bool invert)
{
	return Converter(worldPoint, worldCenter, _toViewFactor, (FPOINT)viewCenter, invert);
}

FPOINT CoordinateConverter::ConverterToWorld(int viewPoint, int viewCenter, FPOINT worldCenter, bool invert)
{
	return Converter((FPOINT)viewPoint, (FPOINT)viewCenter, _toWorldFactor, worldCenter, invert);
}

FPOINT CoordinateConverter::ConvertLengthToView(FPOINT length)
{
	return ConverterToView(length, 0.0f, 0, false);
}

void CoordinateConverter::ConvertWorldToView(const Vector & v, D2D1_POINT_2F& p)
{
	POINT pt = { 0 };
	ConvertWorldToView(v, pt);
	p = D2D1::Point2F((FPOINT)pt.x, (FPOINT)pt.y);
}

void CoordinateConverter::ConvertViewToWorld(const D2D1_POINT_2F& p, Vector& v)
{
	POINT pt = { (int)p.x, (int)p.y };
	ConvertViewToWorld(pt, v);
}

void CoordinateConverter::ConvertWorldToView(const Vector& v, POINT& p)
{
	p =
	{
		(int)ConverterToView(v.x, _worldCenter.x, _viewCenter.x, false),
		(int)ConverterToView(v.y, _worldCenter.y, _viewCenter.y, true)
	};
}

void CoordinateConverter::ConvertViewToWorld(const POINT& p, Vector& v)
{
	v = Vector(
		ConverterToWorld(p.x, _viewCenter.x, _worldCenter.x, false),
		ConverterToWorld(p.y, _viewCenter.y, _worldCenter.y, true));
}