#pragma once
#include <wrl.h>
#include <algorithm>
#include <d2d1_1.h>
#include <d3d11_1.h>
#include <dcomp.h>
#include <dwrite.h>
#include "DataTypes.h"

#pragma comment(lib, "d2d1")
#pragma comment(lib, "d3d11")
#pragma comment(lib, "dxgi")
#pragma comment(lib, "dcomp")
#pragma comment(lib, "Dwrite")

#pragma warning(disable: 4706)
#pragma warning(disable: 4127)

namespace wrl = Microsoft::WRL;
namespace d2d = D2D1;

class RenderDeviceDXGI : public IRenderDevice
{
private:
	//Factories
	wrl::ComPtr<IDWriteFactory>				_dwFactory;
	//dx devices
	wrl::ComPtr<ID3D11Device>				_d3dDevice;
	wrl::ComPtr<ID2D1Device>				_d2dDevice;
	wrl::ComPtr<IDCompositionDesktopDevice>	_dcompDevice;
	wrl::ComPtr<IDCompositionTarget>		_dcompTarget;
	wrl::ComPtr<IDXGISwapChain1>			_dxgiSwapChain;
	wrl::ComPtr<ID2D1DeviceContext>			_dc;
	//device dependent
	wrl::ComPtr<ID2D1SolidColorBrush>		_brushVelocity;
	wrl::ComPtr<ID2D1SolidColorBrush>		_brushGravityInteraction;
	wrl::ComPtr<ID2D1SolidColorBrush>		_brushAuxilliary;
	wrl::ComPtr<ID2D1SolidColorBrush>		_brushCenterMarks;
	//D2d resources
	wrl::ComPtr<ID2D1StrokeStyle>			_stroke;
	wrl::ComPtr<IDWriteTextFormat>			_textFormatSmall;
	wrl::ComPtr<IDWriteTextFormat>			_textFormatMedium;
	//Drawing functions
	void Render();
	void ClearCanvas();
	void DrawPotentialMap();
	void DrawAuxilliary();
	void DrawCenterMarks();
	void DrawBodies();
	void DrawCollisions(Body& b, const unsigned long tick);
	//Auxilliary
	void CreateDevices(HWND hWnd);
	void ReleaseDevices();
	void ResizeSwapChainSurface();
	//Device resources
	void CreateDeviceIndependentResources();
	void CreateDeviceResources();
	void CreateDeviceSizeResources();
	void ReleaseDeviceResources();
public:
	RenderDeviceDXGI();
	~RenderDeviceDXGI();
	//Generics
	virtual void DrawMessage(HWND hWnd, const WCHAR* message);
	//Operations
	virtual void Render(HWND hWnd);
	// Inherited via IRenderDevice
	virtual LRESULT OnCreate(HWND hWnd, WPARAM wParam, LPARAM lParam);
	virtual LRESULT OnPaint(HWND hWnd, WPARAM wParam, LPARAM lParam);
	virtual LRESULT OnDestroy(HWND hWnd, WPARAM wParam, LPARAM lParam);
	virtual LRESULT OnSize(HWND hWnd, WPARAM wParam, LPARAM lParam);
	virtual LRESULT OnDisplayChangeHandler(HWND hWnd, WPARAM wParam, LPARAM lParam);
};

