#include "InteractFunctor.h"
#include "Calculator.h"

InteractFunctor::InteractFunctor(unsigned long t, FPOINT s) : _t(t), _s(s)
{
}

InteractFunctor::~InteractFunctor()
{
}

void InteractFunctor::GravityInteractionOperation(Body& b, const Vector& pos, FPOINT f) const restrict (amp, cpu)
{
	auto& maxInteraction = b.maxGravityInteraction;
	if (f > maxInteraction.force)
	{
		maxInteraction.Set(f, pos);
	}
	else if (maxInteraction.old)
	{
		maxInteraction.Reset();
	}
};

void InteractFunctor::DoBodiesInteraction(Body& b1, const Body& b2, CollisionData& cd, unsigned b1i, unsigned b2i) const restrict(amp, cpu)
{
	if (b2.mass == 0.0f) return; //No interaction
	if (!DoCollisionTest(b1, b2, cd, b1i, b2i))
	{
		auto forceTotal = Calculator::GetForce(b1.pos, b1.mass, b2.pos, b2.mass);
		auto force1 = Vector::Scale(forceTotal, 1.0f / MAX(b1.mass, 1.0f));
		b1.acc = Vector::Add(b1.acc, force1);
		//Adding gravitational interacton
		if (b1.mass <= b2.mass)
		{
			GravityInteractionOperation(b1, b2.pos, Vector::Length(force1));
		}
	}
}

void InteractFunctor::DoCinematicProcessing(Body& b) const restrict(amp, cpu)
{
	//Setting cinamatic values
	b.vel = Vector::Add(b.vel ,Vector::Scale(b.acc, _s));
	b.pos = Vector::Add(b.pos, Vector::Scale(b.vel, _s));
	b.acc = Vector::Zero();
	//Calculating trail
	b.trail.Push(b.pos, _t);
}

void InteractFunctor::DoCollisionOperation(Body& b1, const Body& b2) const restrict(amp, cpu)
{
	if (b1.mass > 0.0f && b2.mass > 0.0f)
	{
		auto v1m = Vector::Scale(b1.vel, b1.mass);
		auto v2m = Vector::Scale(b2.vel, b2.mass);
		b1.mass += b2.mass;
		b1.vel = Vector::Scale(Vector::Add(v1m, v2m), 1.0f / b1.mass);
	}
	b1.collisionTick = _t;
}

unsigned InteractFunctor::DetectCollision(const Body& b1, const Body& b2) const restrict(amp, cpu)
{
	if (b1.mass < b2.mass)
	{
		//First method
		auto d1 = Vector::Sub(b1.pos, b2.pos);
		auto d1L = Vector::Length(d1);
		auto totalRadius = Calculator::GetRadius(b1.mass) + Calculator::GetRadius(b2.mass);
		if (d1L <= totalRadius)
		{
			return 1;
		}
		//Second method
		auto b1NextPos = Vector::Add(b1.pos, Vector::Scale(b1.vel, _s));
		if (Vector::Length(Vector::Sub(b1NextPos, b2.pos)) <= totalRadius)
		{
			return 2;
		}
		//Third method
		auto d2 = Vector::Sub(b1NextPos, b2.pos);
		auto crossed = Vector::Orthogonal(d1).x * Vector::Orthogonal(d2).x < 0.0f;
		if (crossed && Vector::LinePointDistance(b1.pos, b1NextPos, b2.pos) <= totalRadius)
		{
			return 3;
		}
	}

	return 0;
}

bool InteractFunctor::DoCollisionTest(const Body& b1, const Body& b2, CollisionData& cd, unsigned b1i, unsigned b2i) const restrict(amp, cpu)
{
	auto c = DetectCollision(b1, b2);
	if (c > 0)
	{
		cd.to = b2i;
		cd.distance = Vector::Length(Vector::Sub(b1.pos, b2.pos));
		cd.collision = c;
	}

	return cd.collision > 0;
}