#pragma once
#include "DataTypes.h"
#include <Windows.h>

class InteractFunctor
{
private:
	unsigned long		_t = 0L;
	FPOINT				_s = 0.0f;
	//Auxilliary
	unsigned DetectCollision(const Body& b1, const Body& b2) const restrict(amp, cpu);
	bool DoCollisionTest(const Body& b1, const Body& b2, CollisionData& cd, unsigned b1i, unsigned b2i) const restrict(amp, cpu);
	void GravityInteractionOperation(Body& b, const Vector& pos, FPOINT f) const restrict(amp, cpu);
public:
	InteractFunctor(unsigned long tick, FPOINT step);
	~InteractFunctor();
	//Operations
	void DoBodiesInteraction(Body& b1, const Body& b2, CollisionData& cd, unsigned b1i, unsigned b2i) const restrict(amp, cpu);
	void DoCinematicProcessing(Body& b) const restrict(amp, cpu);
	void DoCollisionOperation(Body& b1, const Body& b2) const restrict(amp, cpu);
};