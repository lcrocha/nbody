#include "stdafx.h"
#include "Processor.h"
#include "Calculator.h"
#include "InteractFunctor.h"
#undef max
#undef min
#include <amp_algorithms.h>

Processor::Processor()
{
}

Processor::~Processor()
{
}

bool Processor::Initialize()
{
	return false;
}

bool Processor::Finalize()
{
	return false;
}

struct PotentialAccumulatedData
{
	Potential pontetial;
	FPOINT minForce = 0.0f;
	FPOINT maxForce = 0.0f;

	PotentialAccumulatedData(Potential& p = Potential(), FPOINT min = 0.0f, FPOINT max = 0.0f) restrict(amp, cpu)
	{
		pontetial = p;
		minForce = min;
		maxForce = max;
	}

	PotentialAccumulatedData(PotentialAccumulatedData& p) restrict(amp, cpu)
	{
		pontetial = p.pontetial;
		minForce = p.minForce;
		maxForce = p.maxForce;
	}
};

bool Processor::ProcessPotentialMap(IData* d, Positions& positions, ExecutionTimes& times)
{
	d->SetUpdated(false);
	//Getting sizes
	ChronoTimer timer;
	auto bSize = (unsigned)d->GetTotalBodies();
	auto pSize = (unsigned)positions.size();
	//exiting when no bodies
	if (bSize == 0) return false;
	//Vectors
	BodiesForLock		bb(bSize);
	PositionsForLock	ps(pSize);
	std::vector<PotentialAccumulatedData> dd(pSize);
	//Copying positions
	std::copy(positions.begin(), positions.end(), ps.begin());
	//Locking bodies
	d->LockBodies(bb);
	//Setting arrays
	Concurrency::array_view<Body, 1> bbh(bSize, bb);
	Concurrency::array_view<Vector, 1> psh(pSize, ps);
	Concurrency::array_view<PotentialAccumulatedData, 1> ddh(pSize, dd);
	//Browsing positions
	timer.Start();
	{
		amp_stl_algorithms::transform(amp_stl_algorithms::begin(psh), amp_stl_algorithms::end(psh), amp_stl_algorithms::begin(ddh), [=](const Vector& pos) restrict(amp, cpu)
		{
			auto force = Vector::Zero();
			for (unsigned i = 0; i < bSize; ++i)
			{
				auto& b = bbh[i];
				if (b.mass > 0.0f)
				{
					//Calculating force
					auto f = Calculator::GetForce(pos, 0.0f, b.pos, b.mass);
					//Accumulating force
					force = Vector::Add(force, f);
				}
			}
			//Returning data
			auto fL = Vector::Length(force);
			PotentialAccumulatedData dd(Potential(pos, fL), fL, fL);

			return dd;
		});
		ddh.synchronize();
	}
	times.push_back(timer.Stop().GetTime());
	//Calculating min/max
	PotentialAccumulatedData dmm;
	timer.Start();
	{
		dmm = amp_stl_algorithms::reduce(amp_stl_algorithms::begin(ddh) + 1, amp_stl_algorithms::end(ddh), ddh[0], [=](const PotentialAccumulatedData& dd1, const PotentialAccumulatedData& dd2) restrict(amp, cpu)
		{
			return PotentialAccumulatedData(Potential(), MIN(dd1.minForce, dd2.minForce), MAX(dd1.maxForce, dd2.maxForce));
		});
	}
	times.push_back(timer.Stop().GetTime());
	//Returning data
	timer.Start();
	{
		auto& potentialMap = d->GetPotentialMap();
		potentialMap.potentials.resize(pSize);
		potentialMap.maxPotential = dmm.maxForce;
		potentialMap.minPotential = dmm.minForce;
		concurrency::parallel_for(0, (int)pSize, [&](int i)
		{
			potentialMap.potentials[i] = dd[i].pontetial;
		});
		//Unlock bodies
		d->UnlockBodies(bb, false);
	}
	times.push_back(timer.Stop().GetTime());
	//All done
	d->SetUpdated(true);

	return true;
}

struct BodyAccumulatedData
{
	FPOINT mass = 0.0f;
	Vector com = Vector::Zero();
	Vector center = Vector::Zero();

	BodyAccumulatedData(FPOINT m = 0.0f, const Vector& cm = Vector::Zero(), const Vector& c = Vector::Zero()) restrict(amp, cpu)
	{
		Set(m, cm, c);
	}

	BodyAccumulatedData(const BodyAccumulatedData& bd) restrict(amp, cpu)
	{
		Set(bd.mass, bd.com, bd.center);
	}

	void Set(FPOINT m = 0.0f, const Vector& cm = Vector::Zero(), const Vector& c = Vector::Zero()) restrict(amp, cpu)
	{
		mass = m;
		com = cm;
		center = c;
	}

	BodyAccumulatedData operator+(const BodyAccumulatedData& bd) const restrict(amp, cpu)
	{
		BodyAccumulatedData acc(mass + bd.mass, Vector::Add(com, bd.com), Vector::Add(center, bd.center));

		return acc;
	}
};

struct MaxDistancesData
{
	FPOINT maxCoM = 0.0f;
	FPOINT maxCenter = 0.0f;

	MaxDistancesData(const FPOINT cm = 0.0f, const FPOINT c = 0.0f) restrict (amp, cpu) : maxCoM(cm), maxCenter(c)
	{}
};

bool Processor::ProcessBodies(IData* d, ExecutionTimes& times)
{
	d->SetUpdated(false);
	static ChronoTimer timer;
	//Getting data
	auto size = (unsigned)d->GetTotalBodies();
	//Testing size 0
	if (size == 0)
	{
		d->SetTotalMass(0.0f);
		d->SetCoM(Vector::Zero());
		d->SetCenter(Vector::Zero());
		d->SetMaxCenterDistance(0.0f);
		d->SetMaxCoMDistance(0.0f);
		d->SetUpdated(true);

		return false;
	}
	//Util values
	auto g = d->IsGravity();
	auto tick = (unsigned long)::GetTickCount64();
	InteractFunctor f(tick, d->GetStep());
	timer.Start();
	//Bodies vector
	BodiesForLock bbIn(size);
	BodiesForLock bbOu(size);
	std::vector<BodyAccumulatedData> accs(size);
	CollisionDataForLock cdd(size);
	//Locking bodies
	d->LockBodies(bbIn);
	//Initializing auxilliary vectors
	Concurrency::array_view<BodyAccumulatedData, 1>	acch((int)accs.size(), accs); acch.discard_data();
	Concurrency::array_view<CollisionData, 1>		cddh((int)cdd.size(), cdd); cddh.discard_data();
	times.push_back(timer.Stop().GetTime());
	//Calculating cinematic values
	timer.Start();
	{
		//Initializing bodies vector
		Concurrency::array_view<const Body, 1>			bbInh((int)bbIn.size(), bbIn);
		Concurrency::array_view<Body, 1>				bbOuh((int)bbOu.size(), bbOu);
		//Computing accelerations
		concurrency::parallel_for_each(bbInh.extent, [=](Concurrency::index<1> idx) restrict (amp, cpu)
		{
			auto i = idx[0];
			auto& b1In = bbInh[i];
			auto& b1Ou = bbOuh[i];
			auto& cd1 = cddh[i];
			//Copying data
			b1Ou = b1In;
			b1Ou.acc = Vector::Zero();
			cd1.collision = 0;
			cd1.from = i;
			b1Ou.maxGravityInteraction.Reset();
			for (unsigned j = 0; j < size && g; ++j)
			{
				if (i != j)
				{
					auto& b2In = bbInh[j];
					//Calculating acceleration
					f.DoBodiesInteraction(b1Ou, b2In, cd1, i, j);
				}
			}
			//Testing collision
			if (cd1.collision == 0)
			{
				//Calculating cinematic values
				f.DoCinematicProcessing(b1Ou);
			}
			else
			{
				b1Ou.deleted = true;
			}
			//Accumulating data
			auto& acc = acch[i];
			acc.mass = b1Ou.mass;
			acc.com = (b1Ou.mass > 0.0f ? Vector::Scale(b1Ou.pos, b1Ou.mass) : Vector::Zero());
			acc.center = b1Ou.pos;
		});
		acch.synchronize();
		bbOuh.synchronize();
		cddh.synchronize();
	}
	times.push_back(timer.Stop().GetTime());
	//Processing collision
	timer.Start();
	if (g)
	{
		//Getting only collisions
		auto& begin = cdd.begin();
		auto& end = std::remove_if(cdd.begin(), cdd.end(), [](const CollisionData& cd)
		{
			return cd.collision == 0;
		});
		if (begin != end)
		{
			std::for_each(begin, end, [=, &bbOu, &d](CollisionData& cd)
			{
				auto& b1 = bbOu[cd.to];
				const auto& b2 = bbOu[cd.from];
				f.DoCollisionOperation(b1, b2);
				d->CollisionMethodInc(cd.collision - 1);
			});
		}
	}
	times.push_back(timer.Stop().GetTime());
	//Unlock bodies
	timer.Start();
	d->UnlockBodies(bbOu, true);
	times.push_back(timer.Stop().GetTime());
	//Calculating total mass, CoM and geo center
	timer.Start();
	{
		auto acc = amp_stl_algorithms::reduce(amp_stl_algorithms::begin(acch), amp_stl_algorithms::end(acch), BodyAccumulatedData(), amp_algorithms::plus<BodyAccumulatedData>());
		//Setting values
		d->SetTotalMass(acc.mass);
		if (d->GetTotalMass() > 0.0f)	d->SetCoM(Vector::Scale(acc.com, 1.0f / acc.mass));
		else							d->SetCoM(Vector::Zero());
		if (d->GetTotalBodies() > 0)	d->SetCenter(Vector::Scale(acc.center, 1.0f / size));
		else							d->SetCenter(Vector::Zero());
	}
	times.push_back(timer.Stop().GetTime());
	//Caculating distances
	timer.Start();
	{
		auto center = d->GetCenter();
		auto com = d->GetCoM();
		std::vector<MaxDistancesData> mdds(size);
		Concurrency::array_view<MaxDistancesData, 1> mddh(size, mdds);
		//Retriving distances
		amp_stl_algorithms::transform(amp_stl_algorithms::begin(acch), amp_stl_algorithms::end(acch), amp_stl_algorithms::begin(mddh), [=](const BodyAccumulatedData& bd) restrict(amp, cpu)
		{
			return MaxDistancesData(Vector::Length(Vector::Sub(com, bd.center)), Vector::Length(Vector::Sub(center, bd.center)));
		});
		mddh.synchronize();
		//Reducing
		auto md = amp_stl_algorithms::reduce(amp_stl_algorithms::begin(mddh), amp_stl_algorithms::end(mddh), MaxDistancesData(), [=](const MaxDistancesData& md1, const MaxDistancesData& md2) restrict(amp, cpu)
		{
			return MaxDistancesData(MAX(md1.maxCoM, md2.maxCoM), MAX(md1.maxCenter, md2.maxCenter));
		});
		//Setting values
		d->SetMaxCenterDistance(md.maxCenter);
		d->SetMaxCoMDistance(md.maxCoM);
	}
	times.push_back(timer.Stop().GetTime());
	d->SetUpdated(true);

	return true;
}