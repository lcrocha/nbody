n-Body simulation
tags: C++11, DirectX, Math, Physics, GDI, Kepler laws, Newton laws, Gravitation

-> This is my n-body simulation implementing Newton and Kepler laws using C++11.

Commads:
a, A:			Toggle Cancel/Not Cancel Gravity
c, C:			Toggle Clear/Not Clear canvas
t, T:			Toggle body trail
k, K:			Toggle Cancel/Not Cancel Velocities
<Backspace>:	Reset View distance
8:				Create a massive body towards the center

Modificators for operations below
1 - <Shift>:	Pressed: 		10 operations 
				Not pressed: 	1 operation

2 - <Control>:	Pressed:		Clockwise
				Not pressed:	CounterClockwise

Operations:
+:				[1]	  Decrease View distance
-:				[1]   Increase View distance
<Left>:			[1]   Decrease animation Step
<Right>:		[1]   Increase animation Step
9:				[1]   Increase 10% mass to all
0:				[1]   Decrease 10% mass to all
n:				[1,2] Create masseless body
m:				[1,2] Create massive body
d:				[1]	  Delete body