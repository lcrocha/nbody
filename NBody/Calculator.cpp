#include "stdafx.h"
#include "Calculator.h"

#define funcNormalize(x) ::log2f(x)
#define func1(x) 0.5f*(1.0f + ::sinf(x * 1.5f * (FPOINT)std::_Pi))
#define func2(x) 1.0f*(0.0f + ::cosf((1.0f - x) * 0.5f * (FPOINT)std::_Pi))

D2D1_COLOR_F Calculator::GetColor(FPOINT mass)
{
	static auto maxF = funcNormalize(FLT_MAX);
	static auto minF = funcNormalize(FLT_MIN);
	static auto factor = 0.9f;
	auto cur = (mass > 0.0f ? funcNormalize(mass) : maxF);

	auto comp = (cur - minF) / (maxF - minF);
	auto r = factor*func1(comp) + (1.0f - factor)*comp;
	auto g = (1.0f - factor)*comp;
	auto b = factor*func2(comp) + (1.0f - factor)*comp;
	auto ret = (mass > 0.0f ? D2D1::ColorF(MIN(r, 1.0f), MIN(g, 1.0f), MIN(b, 1.0f)) : D2D1::ColorF(1.0f, 1.0f, 1.0f));

	return ret;
}

D2D1_COLOR_F Calculator::GetColor(FPOINT mass, FPOINT minMass, FPOINT maxMass)
{
	auto comp = (maxMass != minMass ? (mass - minMass) / (maxMass - minMass) : 1.0f);

	return D2D1::ColorF(MIN(comp, 1.0f), MIN(comp, 1.0f), MIN(comp, 1.0f));
}

FPOINT Calculator::GetRadius(FPOINT mass) restrict(amp, cpu)
{
	//return (mass > 0.0f ? 1.0f*Concurrency::precise_math::log10(mass) : 0.0f);
	return (mass > 0.0f ? 1.0f*Concurrency::precise_math::log2(mass) : 0.0f);
}

Vector Calculator::GetForce(const Vector& p1, FPOINT m1, const Vector& p2, FPOINT m2) restrict(amp, cpu)
{
	auto sub0 = Vector::Sub(p2, p1);
	auto dist0 = Vector::Length(sub0);
	auto subNormal = Vector::Normalize(sub0);
	//Testing zero mass
	auto force = Vector::Zero();
	if (m1 > 0.0f && m2 > 0.0f)
	{
		force = Vector::Scale(subNormal, (m1 * m2) / (dist0 * dist0));
	}
	else if (m1 > 0.0f)
	{
		force = Vector::Scale(subNormal, (m1) / (dist0 * dist0));
	}
	else if (m2 > 0.0f)
	{
		force = Vector::Scale(subNormal, (m2) / (dist0 * dist0));
	}

	return force;
}

void Calculator::CalculateOrbitalVelocity(Vector& v, const Vector& p1, const Vector& v1, const Vector& p2, FPOINT m1, bool cw)
{
	auto sub = Vector::Sub(p1, p2);
	auto vNorm = Vector::Normalize(Vector::Orthogonal(sub));
	auto d = Vector::Length(sub);
	v = Vector::Add(v1, Vector::Scale(vNorm, (cw ? 1.0f : -1.0f)*::sqrtf((FPOINT)m1 / ::fabsf(d))));
}