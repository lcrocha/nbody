#pragma once
#include "DataTypes.h"
#include <Windows.h>

class Calculator
{
public:
	//Operations
	static D2D1_COLOR_F GetColor(FPOINT mass);
	static D2D1_COLOR_F GetColor(FPOINT mass, FPOINT minMass, FPOINT maxMass);
	static FPOINT GetRadius(FPOINT mass) restrict(amp, cpu);
	static Vector GetForce(const Vector& p1, FPOINT m1, const Vector& p2, FPOINT m2) restrict(amp, cpu);
	static void CalculateOrbitalVelocity(Vector& v, const Vector& p1, const Vector& v1, const Vector& p2, FPOINT m1, bool cw);
};