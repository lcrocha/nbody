//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by NBody.rc
//
#define IDC_MYICON                      2
#define IDD_NBODY_DIALOG                102
#define IDS_APP_TITLE                   103
#define IDD_ABOUTBOX                    103
#define IDM_ABOUT                       104
#define IDM_EXIT                        105
#define IDI_NBODY                       107
#define IDI_SMALL                       108
#define IDC_NBODY                       109
#define IDR_MAINFRAME                   128
#define IDD_DIALOG1                     129
#define IDD_DLG_GETDISTANCE             133
#define IDC_PY                          1000
#define IDC_PX                          1001
#define IDC_PZ                          1002
#define IDC_VY                          1003
#define IDC_VX                          1004
#define IDC_VZ                          1005
#define IDC_MASS                        1006
#define IDC_BUTTON_POS                  1007
#define IDC_CHECK1                      1008
#define IDC_CHECK_CW                    1008
#define IDC_GETDISTANCEDLG_CW           1008
#define IDC_BUTTON_VEL                  1009
#define IDC_GETDISTANCEDLG_EDIT         1009
#define IDC_BUTTON3                     1010
#define IDC_BUTTON_MASS                 1010
#define IDC_SYSLINK1                    1012
#define IDC_SYSLINK                     1012
#define ID_OPERATIONS_INTERACTIONS      32772
#define ID_OPERATIONS_TRAILS            32773
#define ID_VIEW_S                       32774
#define ID_VIEW_DECREASEDISTANCE        32775
#define ID_VIEW_INCREASEDISTANCE        32776
#define ID_VIEW_RESETDISTANCE           32777
#define ID_VIEW_S32778                  32778
#define ID_VIEW_CL                      32779
#define ID_SIMULATION_INCREASESTEP      32780
#define ID_SIMULATION_DECREASESS        32781
#define ID_SIMULATION_CANCELVELOCITIES  32782
#define ID_BODIES_MASSIVECOLLISION      32783
#define ID_BODIES_CREATEMASSELESSBODY   32784
#define ID_BODIES_CREATEMASSIVEBODY     32785
#define ID_BODIES_DELETEBODY            32786
#define ID_BODIES_S                     32787
#define ID_BODIES_INCREASE10            32788
#define ID_BODIES_D                     32789
#define ID_BODIES_DECREASE10            32790
#define ID_BODIES_S32793                32793
#define ID_BODIES_MULTIPLE10X           32794
#define ID_BODIES_CLOCKWISE             32795
#define ID_SIMULATION_S                 32812
#define ID_SIMULATION_RUNNING           32813
#define ID_VIEW_CLEAR                   32816
#define ID_BODIES_S32818                32818
#define ID_BODIES_NEWBODY               32819
#define ID_VIEW_POTENTIALMAP            32820
#define ID_BODIES_MASSIVEBODY           32823
#define ID_BODIES_MASSIVE               32825
#define ID_FILE_S                       32827
#define ID_FILE_NEWSIMPLESYSTEM         32828
#define ID_FILE_NEWBINARYCIRCULARSYSTEM 32829
#define ID_FILE_NEWBINARYELLIPTICALSYSTEM 32830
#define ID_SIMULATION_GRAVITY           32831
#define ID_VIEW_VELOCITY                32832
#define ID_FILE_NEWTHREEBODYSYSTEM      32836
#define IDC_STATIC                      -1

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        134
#define _APS_NEXT_COMMAND_VALUE         32837
#define _APS_NEXT_CONTROL_VALUE         1014
#define _APS_NEXT_SYMED_VALUE           110
#endif
#endif
