#include "RenderDeviceDXGI.h"
#include "Calculator.h"
#include <sstream>
#include <iomanip>

#define FP std::setprecision(2) << std::setiosflags(4096) << std::setw(9)

#pragma region STATIC_DX_HELPERS
static inline HRESULT CreateD3DDeviceByType(D3D_DRIVER_TYPE const type, wrl::ComPtr<ID3D11Device>& device)
{
	ASSERT(!device);

	UINT flags = D3D11_CREATE_DEVICE_BGRA_SUPPORT;

#if _DEBUG
	//flags |= D3D11_CREATE_DEVICE_DEBUG;
#endif

	return ::D3D11CreateDevice(nullptr, type, nullptr, flags, nullptr, 0, D3D11_SDK_VERSION, device.GetAddressOf(), nullptr, nullptr);
}

static inline wrl::ComPtr<ID3D11Device> CreateD3DDevice()
{
	wrl::ComPtr<ID3D11Device> device;

	auto hr = CreateD3DDeviceByType(D3D_DRIVER_TYPE_HARDWARE, device);
	if (DXGI_ERROR_UNSUPPORTED == hr)
	{
		hr = CreateD3DDeviceByType(D3D_DRIVER_TYPE_WARP, device);
	}
	HR(hr);

	return device;
}

static inline wrl::ComPtr<IDXGIDevice> CreateDXGIDevice(wrl::ComPtr<ID3D11Device> const& d3dDevice)
{
	ASSERT(d3dDevice);

	wrl::ComPtr<IDXGIDevice> dxgiDevice;
	HR(d3dDevice.As(&dxgiDevice));

	return dxgiDevice;
}

static inline wrl::ComPtr<ID2D1Device> CreateD2DDevice(wrl::ComPtr<ID3D11Device> const& d3dDevice)
{
	ASSERT(d3dDevice);

	wrl::ComPtr<IDXGIDevice> dxgiDevice;
	HR(d3dDevice.As(&dxgiDevice));

	wrl::ComPtr<ID2D1Device> d2dDevice;
	HR(::D2D1CreateDevice(dxgiDevice.Get(), nullptr, d2dDevice.GetAddressOf()));

	return d2dDevice;
}

static inline wrl::ComPtr<IDXGIFactory2> CreateDXGIFactory(wrl::ComPtr<ID3D11Device> const& d3dDevice)
{
	ASSERT(d3dDevice);
	wrl::ComPtr<IDXGIDevice> dxgiDevice;
	HR(d3dDevice.As(&dxgiDevice));

	wrl::ComPtr<IDXGIAdapter> dxgiAdapter;
	HR(dxgiDevice->GetAdapter(dxgiAdapter.GetAddressOf()));

	wrl::ComPtr<IDXGIFactory2> dxgiFactory;
	HR(dxgiAdapter->GetParent(__uuidof(dxgiFactory), reinterpret_cast<void**>(dxgiFactory.GetAddressOf())));

	return dxgiFactory;
}

static inline wrl::ComPtr<IDXGISwapChain1> CreateDXGISwapChain(wrl::ComPtr<ID3D11Device> const& d3dDevice, D2D_SIZE_U& size)
{
	ASSERT(d3dDevice);

	wrl::ComPtr<IDXGIFactory2> const& dxgiFactory = CreateDXGIFactory(d3dDevice);

	DXGI_SWAP_CHAIN_DESC1 props = {};
	props.Format = DXGI_FORMAT_B8G8R8A8_UNORM;
	props.SampleDesc.Count = 1;
	props.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	props.BufferCount = 2;
	props.SwapEffect = DXGI_SWAP_EFFECT_FLIP_SEQUENTIAL;
	props.Width = size.width;
	props.Height = size.height;

	wrl::ComPtr<IDXGISwapChain1> dxgiSwapChain;
	HR(dxgiFactory->CreateSwapChainForComposition(d3dDevice.Get(), &props, nullptr, dxgiSwapChain.GetAddressOf()));

	return dxgiSwapChain;
}

static inline wrl::ComPtr<IDCompositionDesktopDevice> CreateDCompDevice(wrl::ComPtr<ID2D1Device> const& d2dDevice)
{
	ASSERT(d2dDevice);
	wrl::ComPtr<IDCompositionDesktopDevice> dcompDevice;
	HR(DCompositionCreateDevice2(d2dDevice.Get(), __uuidof(dcompDevice), reinterpret_cast<void **>(dcompDevice.GetAddressOf())));

	return dcompDevice;
}

static inline wrl::ComPtr<IDCompositionTarget> CreateDCompTarget(HWND hWnd, wrl::ComPtr<IDCompositionDesktopDevice> const& dcompDevice)
{
	ASSERT(dcompDevice);
	wrl::ComPtr<IDCompositionTarget> dcompTarget;
	HR(dcompDevice->CreateTargetForHwnd(hWnd, FALSE, dcompTarget.GetAddressOf()));

	return dcompTarget;
}

static inline wrl::ComPtr<IDCompositionVisual2> CreateDCompVisual(wrl::ComPtr<IDCompositionDesktopDevice> const& dcompDevice)
{
	ASSERT(dcompDevice);
	wrl::ComPtr<IDCompositionVisual2> dcompVisual;
	HR(dcompDevice->CreateVisual(dcompVisual.GetAddressOf()));

	return dcompVisual;
}

static inline wrl::ComPtr<IDCompositionSurface> CreateDCompSurface(D2D_SIZE_U& canvas, wrl::ComPtr<IDCompositionDesktopDevice> const& dcompDevice)
{
	ASSERT(dcompDevice);
	wrl::ComPtr<IDCompositionSurface> dcompSurface;
	HR(dcompDevice->CreateSurface(canvas.width, canvas.height, DXGI_FORMAT_B8G8R8A8_UNORM, DXGI_ALPHA_MODE_PREMULTIPLIED, dcompSurface.GetAddressOf()));

	return dcompSurface;
}

static void SetSwapChainSurfaceToDeviceContext(wrl::ComPtr<IDXGISwapChain1> const& dxgiSwapchain, wrl::ComPtr<ID2D1DeviceContext> const& dc)
{
	ASSERT(dxgiSwapchain);
	ASSERT(dc);

	wrl::ComPtr<IDXGISurface> dxgiSurface;
	HR(dxgiSwapchain->GetBuffer(0, __uuidof(dxgiSurface), reinterpret_cast<void**>(dxgiSurface.GetAddressOf())));
	
	auto const props = d2d::BitmapProperties1(D2D1_BITMAP_OPTIONS_TARGET | D2D1_BITMAP_OPTIONS_CANNOT_DRAW, d2d::PixelFormat(DXGI_FORMAT_B8G8R8A8_UNORM, D2D1_ALPHA_MODE_IGNORE));
	
	wrl::ComPtr<ID2D1Bitmap1> bitmap;
	HR(dc->CreateBitmapFromDxgiSurface(dxgiSurface.Get(), props, bitmap.GetAddressOf()));
	dc->SetTarget(bitmap.Get());
}
#pragma endregion

RenderDeviceDXGI::RenderDeviceDXGI()
{
}

RenderDeviceDXGI::~RenderDeviceDXGI()
{
}

void RenderDeviceDXGI::CreateDevices(HWND hWnd)
{
	if (!_dc)
	{
		//Swap Chain
		auto& rc = _d->GetCanvas();
		_dxgiSwapChain = CreateDXGISwapChain(_d3dDevice, d2d::SizeU(rc.right - rc.left, rc.bottom - rc.top));
		//DirectComposition target
		_dcompTarget = CreateDCompTarget(hWnd, _dcompDevice);
		//DirectComposition visual
		wrl::ComPtr<IDCompositionVisual2> dcompVisual;
		dcompVisual = CreateDCompVisual(_dcompDevice);
		//Direct2d device context
		HR(_d2dDevice->CreateDeviceContext(D2D1_DEVICE_CONTEXT_OPTIONS_NONE, _dc.GetAddressOf()));
		SetSwapChainSurfaceToDeviceContext(_dxgiSwapChain, _dc);
		//Setting DirectComposition resources
		HR(dcompVisual->SetContent(_dxgiSwapChain.Get()));
		HR(_dcompTarget->SetRoot(dcompVisual.Get()));

		CreateDeviceResources();
		CreateDeviceSizeResources();
	}
}

void RenderDeviceDXGI::ResizeSwapChainSurface()
{
	ASSERT(_dc);
	ASSERT(_dxgiSwapChain);

	_dc->SetTarget(nullptr);
	auto& rc = _d->GetCanvas();
	if (S_OK == _dxgiSwapChain->ResizeBuffers(0, rc.right - rc.left, rc.bottom - rc.top, DXGI_FORMAT_UNKNOWN, 0))
	{
		SetSwapChainSurfaceToDeviceContext(_dxgiSwapChain, _dc);
		CreateDeviceSizeResources();
	}
	else
	{
		ReleaseDevices();
	}
}

void RenderDeviceDXGI::ReleaseDevices()
{
	if (_dc)
	{
		ReleaseDeviceResources();
		_dc.Reset();
		_dxgiSwapChain.Reset();
		_dcompTarget.Reset();
	}
}

void RenderDeviceDXGI::CreateDeviceIndependentResources()
{
	//Direct3d device
	_d3dDevice = CreateD3DDevice();
	//Direct2d device
	_d2dDevice = CreateD2DDevice(_d3dDevice);
	//DirectComposition device
	_dcompDevice = CreateDCompDevice(_d2dDevice);
	//Creating D2D1 Factory
	wrl::ComPtr<ID2D1Factory> d2dFactory;
	_d2dDevice->GetFactory(d2dFactory.GetAddressOf());
	//Creating DWrite factory
	HR(::DWriteCreateFactory(DWRITE_FACTORY_TYPE_SHARED, __uuidof(_dwFactory.Get()), reinterpret_cast<IUnknown**>(_dwFactory.GetAddressOf())));
	//Creating stroke
	D2D1_STROKE_STYLE_PROPERTIES props = {};
	props.lineJoin = D2D1_LINE_JOIN_ROUND;
	props.dashStyle = D2D1_DASH_STYLE_DASH;
	props.dashCap = D2D1_CAP_STYLE_ROUND;
	HR(d2dFactory->CreateStrokeStyle(props, nullptr, 0, _stroke.GetAddressOf()));
	//Creating text format
	HR(_dwFactory->CreateTextFormat(L"Tahoma", NULL, DWRITE_FONT_WEIGHT_NORMAL, DWRITE_FONT_STYLE_NORMAL, DWRITE_FONT_STRETCH_NORMAL, 10.0f, L"", _textFormatSmall.GetAddressOf()));
	_textFormatSmall->SetWordWrapping(DWRITE_WORD_WRAPPING_NO_WRAP);
	_textFormatSmall->SetTextAlignment(DWRITE_TEXT_ALIGNMENT_CENTER);
	_textFormatSmall->SetParagraphAlignment(DWRITE_PARAGRAPH_ALIGNMENT_CENTER);
	HR(_dwFactory->CreateTextFormat(L"Tahoma", NULL, DWRITE_FONT_WEIGHT_NORMAL, DWRITE_FONT_STYLE_NORMAL, DWRITE_FONT_STRETCH_NORMAL, 25.0f, L"", _textFormatMedium.GetAddressOf()));
	_textFormatMedium->SetWordWrapping(DWRITE_WORD_WRAPPING_NO_WRAP);
	_textFormatMedium->SetTextAlignment(DWRITE_TEXT_ALIGNMENT_CENTER);
	_textFormatMedium->SetParagraphAlignment(DWRITE_PARAGRAPH_ALIGNMENT_CENTER);
}

void RenderDeviceDXGI::CreateDeviceResources()
{
	HR(_dc->CreateSolidColorBrush(D2D1::ColorF(80.0f / 255.0f, 80.0f / 255.0f, 0.0f), _brushAuxilliary.ReleaseAndGetAddressOf()));
	HR(_dc->CreateSolidColorBrush(D2D1::ColorF(150.0f / 255.0f, 0.0f, 0.0f), _brushGravityInteraction.ReleaseAndGetAddressOf()));
	HR(_dc->CreateSolidColorBrush(D2D1::ColorF(200.0f / 255.0f, 0.0f, 200.0f / 255.0f), _brushVelocity.ReleaseAndGetAddressOf()));
	HR(_dc->CreateSolidColorBrush(D2D1::ColorF(200.0f / 255.0f, 0.0f, 200.0f / 255.0f), _brushCenterMarks.ReleaseAndGetAddressOf()));
}

void RenderDeviceDXGI::CreateDeviceSizeResources()
{
}

void RenderDeviceDXGI::ReleaseDeviceResources()
{
	_brushVelocity.Reset();
	_brushGravityInteraction.Reset();
	_brushAuxilliary.Reset();
	_brushCenterMarks.Reset();
}

//Operations
void RenderDeviceDXGI::Render(HWND hWnd)
{
	//Creating devices if needed
	CreateDevices(hWnd);
	//Rendering
	Render();
	//Presenting
	_dxgiSwapChain->Present(1, 0);
	HR(_dcompDevice->Commit());
}

LRESULT RenderDeviceDXGI::OnPaint(HWND hWnd, WPARAM wParam, LPARAM lParam)
{
	return LRESULT();
}

LRESULT RenderDeviceDXGI::OnCreate(HWND hWnd, WPARAM wParam, LPARAM lParam)
{
	CreateDeviceIndependentResources();

	return LRESULT();
}

LRESULT RenderDeviceDXGI::OnDestroy(HWND hWnd, WPARAM wParam, LPARAM lParam)
{
	ReleaseDevices();
	_dcompDevice.Reset();
	_d2dDevice.Reset();
	_d3dDevice.Reset();

	return LRESULT();
}

LRESULT RenderDeviceDXGI::OnSize(HWND hWnd, WPARAM wParam, LPARAM lParam)
{
	if (_dc && SIZE_MINIMIZED != wParam)
	{
		ResizeSwapChainSurface();
	}
	DrawMessage(hWnd, L"Resizing...");

	return LRESULT();
}

LRESULT RenderDeviceDXGI::OnDisplayChangeHandler(HWND hWnd, WPARAM wParam, LPARAM lParam)
{
	return LRESULT();
}

static void lGetTextRect(std::wstring& text, IDWriteFactory* dwFactory, IDWriteTextFormat* textFormat, D2D_POINT_2F& p, D2D_RECT_F& rc)
{
	auto width = 0.0f;
	auto height = 0.0f;
	//Creating text layout
	Microsoft::WRL::ComPtr<IDWriteTextLayout> textLayout;
	dwFactory->CreateTextLayout(text.c_str(), (UINT32)text.size(), textFormat, 0.0f, 0.0f, textLayout.GetAddressOf());
	//Getting min width and size
	textLayout->DetermineMinWidth(&width);
	textLayout->GetFontSize(0, &height);
	//Setting rect
	rc = { p.x - width / 2.0f, p.y - height / 2.0f, p.x + width / 2.0f, p.y + height / 2.0f };
	rc.left -= height;
	rc.right += height;
}

void RenderDeviceDXGI::Render()
{
	//Begin Draw
	_dc->BeginDraw();
	//Drawing specific method
	if (_d->IsPotentialMap())
	{
		ClearCanvas();
		DrawPotentialMap();
		DrawAuxilliary();
	}
	else
	{
		if (_d->IsClearCanvas())
			ClearCanvas();
		DrawAuxilliary();
		//Drawing bodies
		DrawBodies();
		//Drawing center and com
		DrawCenterMarks();
	}
	//End drawing
	auto hr = _dc->EndDraw();
	if (D2DERR_RECREATE_TARGET == hr)
	{
		ReleaseDevices();
	}
}

void RenderDeviceDXGI::ClearCanvas()
{
	_dc->Clear(D2D1::ColorF(0.0f, 0.0f, 0.0f));
}

void RenderDeviceDXGI::DrawPotentialMap()
{
	auto potentialOffset = _d->GetPotentialOffset();
	auto maxPotential = ::logf(_d->GetPotentialMap().maxPotential);
	auto minPotential = ::logf(_d->GetPotentialMap().minPotential);
	auto calculateFactor = [&minPotential, &maxPotential](FPOINT f)
	{
		return (int)(25 * (FPOINT)(::logf(f) - minPotential) / (maxPotential - minPotential));
	};
	//Browsing potentials
	_d->GetPotentialMap().BrowsePontentials([&](Potential& potential)
	{
		D2D_POINT_2F p = { 0 };
		_cc->ConvertWorldToView(potential.pos, p);
		auto component = calculateFactor(potential.force) / 25.0f;
		if (component > 0.0f)
		{
			wrl::ComPtr<ID2D1SolidColorBrush> brush;
			HR(_dc->CreateSolidColorBrush(D2D1::ColorF(component, component, component), brush.ReleaseAndGetAddressOf()));
			//Drawing
			auto rc = D2D1::RectF(p.x - potentialOffset, p.y - potentialOffset, p.x + potentialOffset, p.y + potentialOffset);
			_dc->FillRectangle(rc, brush.Get());
		}
	});
}

void RenderDeviceDXGI::DrawMessage(HWND hWnd, const WCHAR* message)
{
	std::wstring text(message);
	//Creating devices if needed
	CreateDevices(hWnd);
	//Begin Draw
	_dc->BeginDraw();
	//Drawing text
	ClearCanvas();
	auto& canvas = _d->GetCanvas();
	auto sz = d2d::SizeF((FPOINT)canvas.right - (FPOINT)canvas.left, (FPOINT)canvas.bottom - (FPOINT)canvas.top);
	//auto rc = d2d::RectF(sz.width / 2, sz.height, sz.width / 2, sz.height / 2);
	D2D1_RECT_F rc = {};
	lGetTextRect(text, _dwFactory.Get(), _textFormatMedium.Get(), d2d::Point2F(sz.width / 2.0f, sz.height / 2.0f), rc);
	wrl::ComPtr<ID2D1SolidColorBrush> brush;
	HR(_dc->CreateSolidColorBrush(D2D1::ColorF(d2d::ColorF::White), brush.ReleaseAndGetAddressOf()));
	_dc->DrawText(text.c_str(), (UINT32)text.size(), _textFormatMedium.Get(), rc, brush.Get());
	//End drawing
	auto hr = _dc->EndDraw();
	if (D2DERR_RECREATE_TARGET == hr)
	{
		ReleaseDevices();
	}
	//Presenting
	_dxgiSwapChain->Present(1, 0);
	HR(_dcompDevice->Commit());

}

void RenderDeviceDXGI::DrawAuxilliary()
{
	auto factor = (int)::log10f(_d->GetMaxDistance()) + 1;
	for (int i = factor; (i > factor - 3) && (i > 0); i--)
	{
		SIZE sz = { 0 };
		auto value = ::powf(10.0f, (FPOINT)i);
		auto radius = (FPOINT)_cc->ConvertLengthToView(value);
		D2D1_POINT_2F p;
		_cc->ConvertWorldToView(_cc->GetWorldCenter(), p);
		_dc->DrawEllipse(D2D1::Ellipse(p, radius, radius), _brushAuxilliary.Get(), 1.0f, _stroke.Get());
		if (i > factor - 2)
		{
			D2D_RECT_F rc = {};
			D2D_POINT_2F pt = {};
			//Setting text
			std::wstringstream oss;
			oss << FP << value;
			auto text = oss.str();
			radius += 5.0f;
			//TOP
			pt = { p.x, p.y - radius };
			lGetTextRect(text, _dwFactory.Get(), _textFormatSmall.Get(), pt, rc);
			_dc->SetTransform(D2D1::Matrix3x2F::Rotation(0.0f, pt));
			_dc->DrawTextW(text.c_str(), (UINT32)text.size(), _textFormatSmall.Get(), rc, _brushAuxilliary.Get());
			//RIGHT
			pt = { p.x + radius, p.y };
			lGetTextRect(text, _dwFactory.Get(), _textFormatSmall.Get(), pt, rc);
			_dc->SetTransform(D2D1::Matrix3x2F::Rotation(90.0f, pt));
			_dc->DrawTextW(text.c_str(), (UINT32)text.size(), _textFormatSmall.Get(), rc, _brushAuxilliary.Get());
			//BOTTOM
			pt = { p.x, p.y + radius };
			lGetTextRect(text, _dwFactory.Get(), _textFormatSmall.Get(), pt, rc);
			_dc->SetTransform(D2D1::Matrix3x2F::Rotation(180.0f, pt));
			_dc->DrawTextW(text.c_str(), (UINT32)text.size(), _textFormatSmall.Get(), rc, _brushAuxilliary.Get());
			//LEFT
			pt = { p.x - radius, p.y };
			lGetTextRect(text, _dwFactory.Get(), _textFormatSmall.Get(), pt, rc);
			_dc->SetTransform(D2D1::Matrix3x2F::Rotation(270.0f, pt));
			_dc->DrawTextW(text.c_str(), (UINT32)text.size(), _textFormatSmall.Get(), rc, _brushAuxilliary.Get());
		}
		//Returning transform
		_dc->SetTransform(D2D1::Matrix3x2F::Identity());
	}
}

void RenderDeviceDXGI::DrawCenterMarks()
{
	auto size = 2.0f;
	D2D_POINT_2F p = { 0, 0 };
	_brushCenterMarks->SetColor(D2D1::ColorF(100.0f / 255.0f, 0.0f, 0.0f));
	//Drawing geometric center
	_cc->ConvertWorldToView(_d->GetCenter(), p);
	_dc->FillEllipse(D2D1::Ellipse(p, size, size), _brushCenterMarks.Get());
	//Drawing zero
	_brushCenterMarks->SetColor(D2D1::ColorF(100.0f / 255.0f, 100.0f / 255.0f, 100.0f / 255.0f));
	_cc->ConvertWorldToView(Vector::Zero(), p);
	_dc->FillEllipse(D2D1::Ellipse(p, size, size), _brushCenterMarks.Get());
	//Drawing center o mass
	_brushCenterMarks->SetColor(D2D1::ColorF(100.0f / 255.0f, 100.0f / 255.0f, 0.0f));
	_cc->ConvertWorldToView(_d->GetCoM(), p);
	_dc->FillEllipse(D2D1::Ellipse(p, size, size), _brushCenterMarks.Get());
}

void RenderDeviceDXGI::DrawBodies()
{
	auto tick = (unsigned long)::GetTickCount64();
	SIZE canvas = { (_d->GetCanvas().right - _d->GetCanvas().left), (_d->GetCanvas().bottom - _d->GetCanvas().top) };
	_d->BrowseBodies([&](Body& b)
	{
		wrl::ComPtr<ID2D1SolidColorBrush> brush;
		HR(_dc->CreateSolidColorBrush(D2D1::ColorF(D2D1::ColorF::White), brush.ReleaseAndGetAddressOf()));
		auto dist = Vector::Length(Vector::Sub(b.pos, _cc->GetWorldCenter()));
		//if (dist < _maxDistance)
		{
			D2D_POINT_2F p1;
			_cc->ConvertWorldToView(b.pos, p1);
			if (p1.x < canvas.cx && p1.x > 0 && p1.y < canvas.cy && p1.y > 0)
			{
				//Drawing collision first
				DrawCollisions(b, tick);
				auto radius = MAX(_cc->ConvertLengthToView(Calculator::GetRadius(b.mass)), 1.0f);
				//Drawing trail
				if (_d->IsTrail())
				{
					brush->SetColor(D2D1::ColorF(50.0f / 255.0f, 50.0f / 255.0f, 50.0f / 255.0f));
					auto lastPt = p1;
					for (unsigned int i = 0; i < b.trail.GetSize(); i++)
					{
						auto pos = b.trail.Get(i);
						D2D_POINT_2F p2;
						_cc->ConvertWorldToView(pos, p2);
						_dc->DrawLine(lastPt, p2, brush.Get());
						lastPt = p2;
					}
				}
				//Drawing gravitational interaction
				if (_d->IsGravityInteraction())
				{
					auto& maxInteraction = b.maxGravityInteraction;
					if (maxInteraction.force > 0.0f)
					{
						D2D_POINT_2F p2;
						_cc->ConvertWorldToView(maxInteraction.pos, p2);
						_dc->DrawLine(p1, p2, _brushGravityInteraction.Get());
					}
				}
				b.maxGravityInteraction.old = true;
				//Draw velocity vector
				if (_d->IsVelocity() && Vector::Length(b.vel) > 0.0f)
				{
					//Drawing velocity
					auto norm = Vector::Scale(Vector::Normalize(b.vel), radius + 10.0f);
					D2D_POINT_2F p2 =
					{ p1.x + (int)norm.x, p1.y - (int)norm.y };
					_dc->DrawLine(p1, p2, _brushVelocity.Get());
				}
				//Drawing body
				brush->SetColor(Calculator::GetColor(b.mass));
				_dc->FillEllipse(D2D1::Ellipse(p1, radius, radius), brush.Get());
			}
		}
	}, true);
}

void RenderDeviceDXGI::DrawCollisions(Body& b, const unsigned long tick)
{
	static auto maxTime = 2;
	if (b.collisionTick != 0)
	{
		auto diff = 0.001f*(tick - b.collisionTick);
		if (diff <= maxTime)
		{
			//Calculating factor
			auto factor = diff / maxTime;
			//Setting graphic objects
			wrl::ComPtr<ID2D1SolidColorBrush> brush;
			HR(_dc->CreateSolidColorBrush(D2D1::ColorF(1.0f, 1.0f, 1.0f, 1.0f - factor), brush.ReleaseAndGetAddressOf()));
			//Calculating positions
			auto bodyRadius = Calculator::GetRadius(b.mass);
			auto radius = (FPOINT)_cc->ConvertLengthToView(bodyRadius*(1.2f + 10.0f*factor));
			D2D_POINT_2F p;
			_cc->ConvertWorldToView(b.pos, p);
			//Drawing circle
			_dc->DrawEllipse(D2D1::Ellipse(p, radius, radius), brush.Get());
		}
		else
		{
			b.collisionTick = 0;
		}
	}
}