#pragma once
#include <Windows.h>
#include <d2d1.h>
#include "Vector.h"

class CoordinateConverter
{
private:
	POINT	_viewCenter;
	Vector	_worldCenter;
	FPOINT	_toViewFactor;
	FPOINT	_toWorldFactor;
	//Auxilliary
	FPOINT Converter(FPOINT p1, FPOINT c1, FPOINT factor, FPOINT c2, bool invert);
	FPOINT ConverterToView(FPOINT worldPoint, FPOINT worldCenter, int viewCenter, bool invert);
	FPOINT ConverterToWorld(int viewPoint, int viewCenter, FPOINT worldCenter, bool invert);
public:
	CoordinateConverter();
	//Gets and Sets
	POINT&				GetViewCenter() { return _viewCenter; }
	Vector&				GetWorldCenter()	{ return _worldCenter; }
	//Operations
	void Update(POINT& viewCenter, Vector& worldCenter, FPOINT maxWorldDistance);
	void UpdateMaxDistance(FPOINT maxDistance);
	FPOINT ConvertLengthToView(FPOINT length);
	void ConvertWorldToView(const Vector& v, POINT& p);
	void ConvertViewToWorld(const POINT& p, Vector& v);
	void ConvertWorldToView(const Vector& v, D2D1_POINT_2F& p);
	void ConvertViewToWorld(const D2D1_POINT_2F& p, Vector& v);
};