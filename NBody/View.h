#pragma once
#include <Windows.h>
#include "Data.h"

class View
{
private:
	CoordinateConverter	_cc;
	IData*				_d;
	IRenderDevice*		_r;
	//Auxilliary
	void UpdateCoordinateConverter(const RECT& canvas);
public:
	View();
	virtual ~View();
	//Generics
	void DrawMessage(HWND hWnd, const WCHAR* text);
	CoordinateConverter& GetCoordinateConverter();
	//Operations
	void Initialize(IData* d, IRenderDevice* r);
	void Render(HWND hWnd, ExecutionTimes& times);
	//Windows messages
	virtual LRESULT OnCreate(HWND hWnd, WPARAM wParam, LPARAM lParam);
	virtual LRESULT OnPaint(HWND hWnd, WPARAM wParam, LPARAM lParam);
	virtual LRESULT OnDestroy(HWND hWnd, WPARAM wParam, LPARAM lParam);
	virtual LRESULT OnSize(HWND hWnd, WPARAM wParam, LPARAM lParam);
	virtual LRESULT OnDisplayChangeHandler(HWND hWnd, WPARAM wParam, LPARAM lParam);
};