#include "stdafx.h"
#include "View.h"
#include "stdafx.h"

View::View() :
	_cc(),
	_d(nullptr),
	_r(nullptr)
{
}

View::~View()
{
}

void View::Initialize(IData* d, IRenderDevice* r)
{
	_d = d;
	_r = r;
}

void View::UpdateCoordinateConverter(const RECT& canvas)
{
	POINT viewCenter = { (canvas.right - canvas.left) / 2, (canvas.bottom - canvas.top) / 2 };
	Vector worldCenter = _d->GetCoM();
	//Setting world distance
	if (_d->GetMaxDistance() == 0.0f)
	{
		if (_d->GetTotalBodies() == 1)
		{
			_d->SetMaxDistance(MAX(10.0f*Calculator::GetRadius(_d->GetTotalMass()), 1000.0f));
		}
		else
		{
			_d->SetMaxDistance(1.0f*_d->GetMaxCoMDistance());
		}
	}
	//Updating coordinate converting tool
	_cc.Update(viewCenter, worldCenter, _d->GetMaxDistance());
}

void View::DrawMessage(HWND hWnd, const WCHAR* text)
{
	_r->DrawMessage(hWnd, text);
}

CoordinateConverter& View::GetCoordinateConverter()
{
	UpdateCoordinateConverter(_d->GetCanvas());

	return _cc;
}

void View::Render(HWND hWnd, ExecutionTimes& times)
{
	static ChronoTimer timer;
	timer.Start();
	UpdateCoordinateConverter(_d->GetCanvas());
	times.push_back(timer.Stop().GetTime());

	timer.Start();
	if (_d->IsUpdated())
	{
		_r->Render(hWnd);
		_d->SetUpdated(false);
	}
	times.push_back(timer.Stop().GetTime());
}

LRESULT View::OnCreate(HWND hWnd, WPARAM wParam, LPARAM lParam)
{
	_r->SetData(_d, &_cc);

	return _r->OnCreate(hWnd, wParam, lParam);
}

LRESULT View::OnPaint(HWND hWnd, WPARAM wParam, LPARAM lParam)
{
	return _r->OnPaint(hWnd, wParam, lParam);
}

LRESULT View::OnDestroy(HWND hWnd, WPARAM wParam, LPARAM lParam)
{
	return _r->OnDestroy(hWnd, wParam, lParam);
}

LRESULT View::OnSize(HWND hWnd, WPARAM wParam, LPARAM lParam)
{
	return _r->OnSize(hWnd, wParam, lParam);
}

LRESULT View::OnDisplayChangeHandler(HWND hWnd, WPARAM wParam, LPARAM lParam)
{
	return _r->OnDisplayChangeHandler(hWnd, wParam, lParam);
}
