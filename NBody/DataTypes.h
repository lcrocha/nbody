#pragma once
#include <list>
#include <algorithm>
#include <functional>
#include <random>
#include <ppl.h>
#include <concurrent_vector.h>
#include "CoordinateConverter.h"

#define ASSERT(expression) _ASSERTE(expression)

#if _DEBUG
#define VERIFY(expression) ASSERT(expression)
#define HR(expression) ASSERT(S_OK == (expression))

inline void TRACE(WCHAR const* const format, ...)
{
	va_list args;
	va_start(args, format);
	WCHAR output[512] = { L"" };
	vswprintf_s(output, format, args);
	::OutputDebugString(output);
	va_end(args);
}
#else
#define VERIFY(expression) (expression)

struct ComException
{
	HRESULT const hr;
	ComException(HRESULT const value) : hr(value) {}
};

inline void HR(HRESULT const hr)
{
	if (S_OK != hr)
		throw ComException(hr);
}

#define TRACE __noop
#endif

static bool or (bool v1, bool v2)
{
	return v1 || v2;
}

template<typename T>
static T Greater(T d1, T d2)
{
	return MAX(d1, d2);
}

template<typename T>
static T Lesser(T d1, T d2)
{
	return MIN(d1, d2);
}

typedef concurrency::concurrent_vector<Vector> Positions;
typedef std::vector<Vector> PositionsForLock;

struct Potential
{
	Vector pos = Vector::Zero();
	FPOINT force = 0.0f;

	Potential(const Vector& p = Vector::Zero(), FPOINT f = 0.0f) restrict(amp, cpu)
	{
		pos = p;
		force = f;
	}

	Potential(const Potential& p) restrict(amp, cpu)
	{
		pos = p.pos;
		force = p.force;
	}

	Potential operator > (const Potential& p) const restrict(amp, cpu)
	{
		return (force > p.force ? *this : p);
	}

	Potential operator < (const Potential& p) const restrict(amp, cpu)
	{
		return (force < p.force ? *this : p);
	}
};

typedef concurrency::concurrent_vector<Potential> Potentials;

struct PotentialMap
{
	Potentials potentials;
	FPOINT maxPotential;
	FPOINT minPotential;

	void BrowsePontentials(std::function<void(Potential&)> f)
	{
		concurrency::parallel_for_each(potentials.begin(), potentials.end(), f);
	}

	void Clear()
	{
		potentials.clear();
		maxPotential = 0.0f;
		minPotential = 0.0f;
	}
};

struct Trail
{
private:
	static const unsigned int Size = 10;
	static const unsigned int _time = 100;
	unsigned int _maxI = 0;
	unsigned int _i = 0;
	unsigned long _last = 0L;
	Vector _trails[Size] = { Vector::Zero() };
	//Auxilliary
	unsigned int GetIndex(unsigned int i) const restrict (amp, cpu)
	{
		return (_i + i) % Size;
	}

public:
	Trail() restrict(amp, cpu)
	{
	}

	Trail(const Trail& t) restrict(amp, cpu)
	{
		_last = t._last;
		_i = t._i;
		_maxI = t._maxI;
		for (int i = 0; i < Size; ++i)
		{
			_trails[i] = t._trails[i];
		}
	}

	const Vector& Get(unsigned int i) const restrict(amp, cpu)
	{
		return _trails[GetIndex(Size - i - 1)];
	}

	void Push(const Vector& pos, unsigned long t) restrict (amp, cpu)
	{
		if (t - _last > _time)
		{
			if (_maxI < Size)
			{
				_maxI++;
			}
			_trails[_i] = pos;
			_i = GetIndex(1);
			_last = t;
		}
	}

	unsigned int GetSize() restrict(amp, cpu)
	{
		return _maxI;
	}
};


struct GravityInteraction
{
	bool		old = false;
	Vector		pos = Vector::Zero();
	FPOINT		force = 0.0f;

	GravityInteraction(FPOINT f = 0.0f, const Vector& p = Vector::Zero()) restrict(amp, cpu)
	{
		Set(f, pos);
	}

	GravityInteraction(const GravityInteraction& gi) restrict(amp, cpu)
	{
		old = gi.old;
		pos = gi.pos;
		force = gi.force;
	}

	void Set(FPOINT f, const Vector& p) restrict(amp, cpu)
	{
		old = false;
		force = f;
		pos = p;
	}

	void Reset() restrict(amp, cpu)
	{
		Set(0.0f, Vector::Zero());
	}
};

struct Body
{
public:
	bool							deleted = false;
	Vector							pos = Vector::Zero();
	Vector							vel = Vector::Zero();
	Vector							acc = Vector::Zero();
	FPOINT							mass = 0.0f;
	long							collisionTick = 0L;
	Trail							trail;
	GravityInteraction				maxGravityInteraction;

	Body() restrict(amp, cpu): trail(), maxGravityInteraction()
	{
	}

	Body(const Body& b) restrict(amp, cpu)
	{
		deleted = b.deleted;
		pos = b.pos;
		vel = b.vel;
		acc = b.acc;
		mass = b.mass;
		collisionTick = b.collisionTick;
		trail = b.trail;
		maxGravityInteraction = b.maxGravityInteraction;
	}

	bool IsDeleted() const restrict(amp, cpu)
	{
		return deleted;
	}

	void SetDeleted() restrict(amp, cpu)
	{
		deleted = true;
	}
};

typedef concurrency::concurrent_vector<Body>	Bodies;
typedef std::vector<Body>						BodiesForLock;

struct CollisionData
{
	unsigned collision = 0;
	unsigned to = 0;
	unsigned from = 0;
	FPOINT distance = 0.0f;

	CollisionData(unsigned t = 0, unsigned f = 0, FPOINT d = 0.0f) restrict (amp, cpu): to(t), from(f), distance(d)
	{
		collision = 0;
	}
};

typedef std::vector<CollisionData>	CollisionDataForLock;

class ChronoTimer
{
private:
	std::chrono::time_point<std::chrono::system_clock> _start;
	std::chrono::time_point<std::chrono::system_clock> _stop;

	unsigned long GetTime(std::chrono::time_point<std::chrono::system_clock>& t)
	{
		return (unsigned long)std::chrono::duration_cast<std::chrono::milliseconds>(t - _start).count();
	}

public:
	ChronoTimer()
	{
		Start();
	}

	void Start()
	{
		_start = std::chrono::system_clock::now();
	}

	ChronoTimer& Stop()
	{
		_stop = std::chrono::system_clock::now();

		return *this;
	}

	unsigned long GetTime()
	{
		return GetTime(_stop);
	}

	unsigned long GetPartialTime()
	{
		return GetTime(std::chrono::system_clock::now());
	}
};

typedef std::function<void(Body&, Body&)>	InteractBodiesFunction;
typedef std::function<void(Body&)>			BrowseBodiesFunction;

class IData
{
public:
	//Values
	virtual unsigned GetPotentialOffset() = 0;
	virtual FPOINT GetMaxDistance() = 0;
	virtual const RECT& GetCanvas() = 0;
	virtual const Vector& GetCenter() = 0;
	virtual const Vector& GetCoM() = 0;
	virtual unsigned GetTotalBodies() = 0;
	virtual FPOINT GetTotalMass() = 0;
	virtual FPOINT GetMaxCoMDistance() = 0;
	virtual PotentialMap& GetPotentialMap() = 0;
	virtual FPOINT GetStep() = 0;
	virtual unsigned GetTrailLength() = 0;
	virtual unsigned CollisionMethodTotal() = 0;
	virtual unsigned CollisionMethod(unsigned c) = 0;
	//Check data
	virtual bool IsUpdated() = 0;
	virtual bool IsTrail() = 0;
	virtual bool IsGravityInteraction() = 0;
	virtual bool IsVelocity() = 0;
	virtual bool IsClearCanvas() = 0;
	virtual bool IsPotentialMap() = 0;
	virtual bool IsGravity() = 0;
	//Bodies base
	virtual void LockBodies(BodiesForLock& bb) = 0;
	virtual void UnlockBodies(BodiesForLock& bb, bool update) = 0;
	//Loops
	virtual bool BrowseBodies(BrowseBodiesFunction f, bool deleteBodies = false) = 0;
	virtual bool InteractBodies(InteractBodiesFunction f) = 0;
	//Set data
	virtual void SetUpdated(bool u) = 0;
	virtual void SetMaxDistance(FPOINT d) = 0;
	virtual void SetMaxCoMDistance(const FPOINT d) = 0;
	virtual void SetMaxCenterDistance(const FPOINT d) = 0;
	virtual void SetCenter(Vector& center) = 0;
	virtual void SetCoM(Vector& coM) = 0;
	virtual void SetTotalMass(FPOINT m) = 0;
	virtual void CollisionMethodInc(unsigned c) = 0;
};

typedef std::vector<unsigned long>	ExecutionTimes;

class IRenderDevice
{
protected:
	IData* _d = nullptr;
	CoordinateConverter* _cc = nullptr;
public:
	//Generic
	virtual void DrawMessage(HWND hWnd, const WCHAR* text) = 0;
	void SetData(IData* d, CoordinateConverter* cc) { _d = d; _cc = cc; }
	//Operations
	virtual void Render(HWND hWnd) = 0;
	//Windows messages
	virtual LRESULT OnCreate(HWND hWnd, WPARAM wParam, LPARAM lParam) = 0;
	virtual LRESULT OnPaint(HWND hWnd, WPARAM wParam, LPARAM lParam) = 0;
	virtual LRESULT OnDestroy(HWND hWnd, WPARAM wParam, LPARAM lParam) = 0;
	virtual LRESULT OnSize(HWND hWnd, WPARAM wParam, LPARAM lParam) = 0;
	virtual LRESULT OnDisplayChangeHandler(HWND hWnd, WPARAM wParam, LPARAM lParam) = 0;
};

class RNG //Random Number Generator
{
private:
	std::random_device _rd;
	std::mt19937 _gen;
	RNG() : _gen(_rd())
	{
	}
public:
	static RNG& Get()
	{
		static RNG rng;

		return rng;
	}

	unsigned int RandUINT()
	{
		std::uniform_int_distribution<unsigned int> dist;

		return dist(_gen);
	}

	FPOINT RandFPOINT()
	{
		/*std::uniform_real_distribution<FPOINT> dist(0.0f, 1.0f);
		return dist(_gen);*/
		return std::generate_canonical<FPOINT, 10>(_gen);
	}
};