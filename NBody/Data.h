#pragma once
#include <functional>
#include "DataTypes.h"
#include "Calculator.h"

typedef std::vector<unsigned>	Collisions;

class Data : public IData
{
private:
	Bodies				_bodies;
	Vector				_coM;
	Vector				_center;
	FPOINT				_maxCenterDistance;
	FPOINT				_maxCoMDistance;
	unsigned			_trailLength;
	FPOINT				_totalMass;
	PotentialMap		_potentialMap;
	bool				_updated;
	//View configuration
	bool				_potential;
	bool				_gravitationalInteraction;
	bool				_trail;
	bool				_velocity;
	bool				_clearCanvas;
	FPOINT				_maxDistance;
	unsigned			_potentialOffset;
	RECT				_canvas;
	FPOINT				_step;
	bool				_gravity;
	Collisions			_collisions;
	//Auxilliary
	void ClearData();
	FPOINT GetMassiveBodyMass();
	bool GetGreaterMassBody(std::function<void(Body&)> f);
	void CreateNewBody(Vector& pos, FPOINT m, bool cw);
	void PushBody(Body& b);
public:
	//Constructor/Destructor
	Data();
	~Data();
	//Initializing/Finalizing
	bool Initialize();
	bool Finalize();
	//Gets and sets
	FPOINT GetMaxDistance() { return _maxDistance; }
	void SetMaxDistance(FPOINT d) { _maxDistance = d; }
	RECT& GetCanvas() { return _canvas; }
	bool IsGravityInteraction() { return _gravitationalInteraction; }
	bool IsTrail() { return _trail; }
	bool IsVelocity() { return _velocity; }
	bool IsPotentialMap() { return _potential; }
	bool IsClearCanvas() { return _clearCanvas; }
	unsigned GetPotentialOffset() { return _potentialOffset; }
	void SetPotentialOffset(int o) { _potentialOffset = o; }
	void SetCanvas(RECT& rc) { _canvas = rc; }
	const Vector& GetCoM() { return _coM; }
	void SetCoM(Vector& coM) { _coM = coM; }
	const Vector& GetCenter() { return _center; }
	void SetCenter(Vector& center) { _center = center; }
	const FPOINT GetMaxCenterDistance() { return _maxCenterDistance; }
	void SetMaxCenterDistance(const FPOINT d) { _maxCenterDistance = d; }
	virtual FPOINT GetMaxCoMDistance() { return _maxCoMDistance; }
	void SetMaxCoMDistance(const FPOINT d) { _maxCoMDistance = d; }
	unsigned GetTotalBodies();
	unsigned GetTrailLength() { return _trailLength; }
	FPOINT GetTotalMass() { return _totalMass; }
	void SetTotalMass(FPOINT m) { _totalMass = m; }
	bool GetMostAttractiveBody(Vector& pos, Body& b);
	PotentialMap& GetPotentialMap() { return _potentialMap; }
	bool IsUpdated() { return _updated; }
	void SetUpdated(bool u) { _updated = u; }
	bool IsGravity() { return _gravity; }
	FPOINT GetStep() { return _step; }
	void SetStep(FPOINT s) { _step = s; }
	unsigned CollisionMethodTotal();
	unsigned CollisionMethod(unsigned c);
	void CollisionMethodInc(unsigned c);
	//Templates
	void TemplateSimpleSystem();
	void TemplateBinaryCircularSystem(FPOINT r, bool cw);
	void TemplateBinaryElipticalSystem(FPOINT r, bool cw);
	void TemplateThreeBodySystem(FPOINT r, bool cw);
	//Bodies base
	void LockBodies(BodiesForLock& bb);
	void UnlockBodies(BodiesForLock& bb, bool update);
	//Interacting bodies
	bool BrowseBodies(BrowseBodiesFunction f, bool deleteBodies = false);
	bool InteractBodies(InteractBodiesFunction f);
	//Toggle
	void KillVelocity();
	void IncreaseFX();
	void DecreaseFX();
	void CreateNewBody(int mode, bool cw, Vector& lt, Vector& rb, bool orbitalVelocity);
	void CreateNewBody(Vector& pos, bool massive, bool cw);
	void CreateNewBody(Vector& p, Vector& v, FPOINT m);
	void DeleteBody(unsigned int qty);
	void DoMassiveCollision(FPOINT distance, int qty);
	void ToggleVelocity() { _velocity = !_velocity; }
	void ToggleTrail() { _trail = !_trail; }
	void ToggleGravitationalInteraction() { _gravitationalInteraction = !_gravitationalInteraction; }
	void ToggleClearCanvas() { _clearCanvas = !_clearCanvas; }
	void TogglePotentialMap() { _potential = !_potential; }
	void ToggleGravity() { _gravity = !_gravity; }
	void IncreaseStep() { _step *= 10.0f; }
	void DecreaseStep() { _step /= 10.0f; }
	void IncreaseDistance() { _maxDistance *= 1.1f; }
	void DecreaseDistance() { _maxDistance *= 0.9f; }
	void ResetDistance() { _maxDistance = 0.0f; }
};