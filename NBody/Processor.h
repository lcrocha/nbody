#pragma once
#include "DataTypes.h"
#include "Data.h"

class Processor
{
public:
	Processor();
	~Processor();
	//Initiliazing/Finalizing
	bool Initialize();
	bool Finalize();
	//Process
	bool ProcessPotentialMap(IData* d, Positions& positions, ExecutionTimes& times);
	bool ProcessBodies(IData* d, ExecutionTimes& times);
};

