// NBody.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "NBody.h"
#include "Data.h"
#include "Processor.h"
#include "RenderDeviceDXGI.h"
#include "View.h"
#include <Windows.h>
#include <windowsx.h>
#include <sstream>
#include <iomanip>
#include <commctrl.h>
#include <shellapi.h>
#include <ppl.h>

#pragma comment(lib, "MinCore")

#define MAX_LOADSTRING 100

#define WM_UPDATE_STATUSBAR			WM_USER + 1

bool _visible = true;
bool _running = true;
SIZE _minWindowSize = { 600, 400 };
// Global Variables:
HINSTANCE hInst;                                // current instance
WCHAR szTitle[MAX_LOADSTRING];                  // The title bar text
WCHAR szWindowClass[MAX_LOADSTRING];            // the main window class name

struct Engine
{
	Processor p;
	Data d;
	View v;
	ExecutionTimes pT;
	ExecutionTimes vT;

	Engine()
	{
		static RenderDeviceDXGI device;
		d.Initialize();
		p.Initialize();
		v.Initialize(&d, &device);
	}

	~Engine()
	{
		//v.Finalize();
		p.Finalize();
		d.Finalize();
	}
};

// Forward declarations of functions included in this code module:
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int, HWND&, LPVOID param);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    AboutDlgProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    NewBodyDlgProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    GetDistanceDlgProc(HWND, UINT, WPARAM, LPARAM);
std::wstring		lGetVersion(WCHAR* entry);

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPWSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

	HWND hWnd = 0L;

	// Initialize global strings
    LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
    LoadStringW(hInstance, IDC_NBODY, szWindowClass, MAX_LOADSTRING);
    MyRegisterClass(hInstance);

	Engine e;
	// Perform application initialization:
    if (!InitInstance(hInstance, nCmdShow, hWnd, (LPVOID)&e))
    {
        return FALSE;
    }

    HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_NBODY));

	MSG msg = {};

    // Main message loop:
	while (msg.message != WM_QUIT)
	{
		if (_running && _visible)
		{
			if (!e.d.IsPotentialMap())
			{
				//Processing bodies
				{
					e.pT.clear();
					e.p.ProcessBodies(&e.d, e.pT);
				}
				//Rendering
				{
					e.vT.clear();
					e.v.Render(hWnd, e.vT);
				}
				static ChronoTimer timer;
				static ChronoTimer fps;
				if (timer.GetPartialTime() > 500)
				{
					timer.Stop().Start();
					::PostMessage(hWnd, WM_UPDATE_STATUSBAR, (WPARAM)fps.Stop().GetTime(), 0L);
				}
				fps.Start();
			}
			//Process all pending messages
			while (::PeekMessage(&msg, nullptr, 0, 0, PM_REMOVE) && msg.message != WM_QUIT)
			{
				if (!::TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
				{
					::TranslateMessage(&msg);
					::DispatchMessage(&msg);
				}
			}
		}
		else
		{
			if (::GetMessage(&msg, nullptr, 0, 0) && msg.message != WM_QUIT)
			{
				if (!::TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
				{
					::TranslateMessage(&msg);
					::DispatchMessage(&msg);
				}
			}
		}
	}

    return (int) msg.wParam;
}


//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
    WNDCLASSEXW wcex;

    wcex.cbSize = sizeof(WNDCLASSEX);

    wcex.style          = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc    = WndProc;
    wcex.cbClsExtra     = 0;
    wcex.cbWndExtra     = 0;
    wcex.hInstance      = hInstance;
    wcex.hIcon          = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_NBODY));
    wcex.hCursor        = LoadCursor(nullptr, IDC_ARROW);
    wcex.hbrBackground  = (HBRUSH)(COLOR_WINDOW+1);
    wcex.lpszMenuName   = MAKEINTRESOURCEW(IDC_NBODY);
    wcex.lpszClassName  = szWindowClass;
    wcex.hIconSm        = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

    return RegisterClassExW(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow, HWND& hWnd, LPVOID param)
{
   hInst = hInstance; // Store instance handle in our global variable

   hWnd = CreateWindow(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
      CW_USEDEFAULT, 0,_minWindowSize.cx, _minWindowSize.cy, nullptr, nullptr, hInstance, param);

   if (!hWnd)
   {
      return FALSE;
   }

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return TRUE;
}

static void lUpdateMenu(HMENU menu, Data& d, bool cw, bool multiple, bool massive)
{
	if (0L != menu)
	{
		::CheckMenuItem(menu, ID_SIMULATION_GRAVITY, (d.IsGravity() ? MF_CHECKED : MF_UNCHECKED));
		::CheckMenuItem(menu, ID_OPERATIONS_INTERACTIONS, (d.IsGravityInteraction() ? MF_CHECKED : MF_UNCHECKED));
		::CheckMenuItem(menu, ID_OPERATIONS_TRAILS, (d.IsTrail() ? MF_CHECKED : MF_UNCHECKED));
		::CheckMenuItem(menu, ID_VIEW_VELOCITY, (d.IsVelocity() ? MF_CHECKED : MF_UNCHECKED));
		::CheckMenuItem(menu, ID_BODIES_CLOCKWISE, (cw ? MF_CHECKED : MF_UNCHECKED));
		::CheckMenuItem(menu, ID_BODIES_MULTIPLE10X, (multiple ? MF_CHECKED : MF_UNCHECKED));
		::CheckMenuItem(menu, ID_SIMULATION_RUNNING, (_running ? MF_CHECKED : MF_UNCHECKED));
		::CheckMenuItem(menu, ID_VIEW_POTENTIALMAP, (d.IsPotentialMap() ? MF_CHECKED : MF_UNCHECKED));
		::CheckMenuItem(menu, ID_BODIES_MASSIVE, (massive ? MF_CHECKED : MF_UNCHECKED));
	}
}

RECT lUpdateClientRect(HWND hWnd, HWND hWndStatusBar)
{
	RECT rc = { 0 };
	RECT rcStatusBar = { 0 };

	::GetClientRect(hWnd, &rc);
	::GetClientRect(hWndStatusBar, &rcStatusBar);
	rc.bottom -= rcStatusBar.bottom;

	return rc;
}


void lUpdatePositionsMap(Positions& positions, HWND hWnd, HWND hWndStatusBar, CoordinateConverter& cc, int offset)
{
	//Getting size
	RECT client = lUpdateClientRect(hWnd, hWndStatusBar);
	SIZE sz = { client.right - client.left, client.bottom - client.top };
	//Mapping points
	auto cx = sz.cx / offset;
	auto cy = sz.cy / offset;
	concurrency::parallel_for(0L, cx, [=,&positions, &cc](auto i)
	{
		concurrency::parallel_for(0L, cy, [=, &positions, &cc](auto j)
		{
			POINT p = { client.left + (long)offset*(long)(i + 0.5f), client.top + (long)offset*(long)(j + 0.5f) };
			Vector v;
			cc.ConvertViewToWorld(p, v);
			positions.push_back(v);
		});
	});
}

static HWND lCreateStatusBar(HWND hWnd)
{
	auto ret = ::CreateWindowEx(0, STATUSCLASSNAME, NULL, WS_CHILD | WS_VISIBLE | SBARS_SIZEGRIP, 0, 0, 0, 0, hWnd, 0L, hInst, NULL);
	int iStatusWidths[] = { 75, 160, 260, 320, 480, -1 };
	auto size = (sizeof(iStatusWidths) / sizeof(int));
	::SendMessage(ret, SB_SETPARTS, (WPARAM)size, (LPARAM)iStatusWidths);
	::ShowWindow(ret, SW_SHOW);

	return ret;
}

#define SB_CUSTOM_SLOT	5

static int lUpdateStatusBarSlotText(HWND h, int slot, const char* text)
{
	auto ret = (int)::SendMessageA(h, SB_SETTEXTA, slot, (LPARAM)text);
	::ShowWindow(h, SW_SHOW);

	return ret;
}

static int lUpdateStatusBarCustomInformation(HWND h, const char* text)
{
	return lUpdateStatusBarSlotText(h, SB_CUSTOM_SLOT, text);
}

std::string GetExecutionTimes(ExecutionTimes& times)
{
	std::ostringstream oss;
	auto size = (unsigned)times.size();
	//Begin
	oss << "<";
	//Looping times
	for (auto i = (unsigned)0; i < size; ++i)
	{
		oss << times[i];
		if (i < size - 1)
		{
			oss << ",";
		}
	}
	//End
	oss << ">";

	return oss.str();
}

static void lUpdateStatusBar(HWND h, Data& d, bool cw, bool multiple, bool massive, FPOINT fps, ExecutionTimes& pTimes, ExecutionTimes& vTimes)
{
	//Copying collisions
	static ExecutionTimes cTimes;
	cTimes.resize(d.CollisionMethodTotal());
	for (unsigned i = 0; i < cTimes.size(); ++i)
	{
		cTimes[i] = d.CollisionMethod(i);
	}
	char text[256] = { "" };
	//Setting slot0
	sprintf_s(text, "Bodies: %d", d.GetTotalBodies());
	lUpdateStatusBarSlotText(h, 0, text);
	//Setting slot1
	sprintf_s(text, "Step: %0.2e", (FPOINT)d.GetStep());
	lUpdateStatusBarSlotText(h, 1, text);
	//Setting slot2
	sprintf_s(text, "T. Mass: %0.2e", (FPOINT)d.GetTotalMass());
	lUpdateStatusBarSlotText(h, 2, text);
	//Setting slot3
	sprintf_s(text, "FPS: %0.2f", (FPOINT)fps);
	lUpdateStatusBarSlotText(h, 3, text);
	//Setting slot4
	sprintf_s(text, "CoM[%0.2e,%0.2e]",
		(FPOINT)d.GetCoM().x,
		(FPOINT)d.GetCoM().y);
	lUpdateStatusBarSlotText(h, 4, text);
	//Setting Custom
	sprintf_s(
		text,
		"p%s v%s c%s <%s> <%s> <%s> <%s> <%s> <%s> <%s>",
		GetExecutionTimes(pTimes).c_str(),
		GetExecutionTimes(vTimes).c_str(),
		GetExecutionTimes(cTimes).c_str(),
		(_running ? "Running" : "Paused"),
		(cw ? "Clockwise" : "CounterClockwise"),
		(multiple ? "Multiple" : "Single"),
		(massive ? "Massive" : "Masseless"),
		(d.IsTrail() ? "Trail" : "No Trail"),
		(d.IsVelocity() ? "Velocity" : "No Velocity"),
		(d.IsGravityInteraction() ? "GInteraction" : "No GInteraction"));
	lUpdateStatusBarCustomInformation(h, text);
}

void ProcessPotentialMap(Processor& p, Data& d, View& v, HWND hWnd, HWND hWndStatusBar, bool visible)
{
	if (visible)
	{
		ExecutionTimes pTimes;
		ExecutionTimes vTimes;
		Positions positions;
		ChronoTimer timer;
		//Updating statusbar
		lUpdateStatusBarCustomInformation(hWndStatusBar, "Starting potential map...");
		v.DrawMessage(hWnd, L"Starting potential map...");
		//Processing potential map
		lUpdatePositionsMap(positions, hWnd, hWndStatusBar, v.GetCoordinateConverter(), d.GetPotentialOffset());
		pTimes.push_back(timer.Stop().GetTime());
		p.ProcessPotentialMap(&d, positions, pTimes);
		v.Render(hWnd, vTimes);
		//Updating statusbar
		char msg[256] = { "" };
		sprintf_s(msg, "Potential map done in p%s v%s", GetExecutionTimes(pTimes).c_str(), GetExecutionTimes(vTimes).c_str());
		lUpdateStatusBarCustomInformation(hWndStatusBar, msg);
	}
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND  - process the application menu
//  WM_PAINT    - Paint the main window
//  WM_DESTROY  - post a quit message and return
//
//

LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	static Engine* e = nullptr;
	static HMENU menu = 0L;
	static bool massive = false;
	static bool cw = true;
	static bool multiple = true;
	static HWND hWndStatusBar = 0L;
	static RECT rcClient = { 0 };
	//Updating helpers
	static auto updateStatusBar = [&](FPOINT fps) { lUpdateStatusBar(hWndStatusBar, e->d, cw, multiple, massive, fps, e->pT, e->vT); };
	static auto updateMenu = [&]() { lUpdateMenu(menu, e->d, cw, multiple, massive); };

    switch (message)
    {
    case WM_COMMAND:
	{
		auto multQty = 10;
		std::function<void(void)> f;
		int wmId = LOWORD(wParam);
		// Parse the menu selections:
		switch (wmId)
		{
		case IDM_ABOUT:
			::DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, AboutDlgProc);
			break;
		case IDM_EXIT:
			::DestroyWindow(hWnd);
			break;
		case ID_FILE_NEWSIMPLESYSTEM:
			e->d.ResetDistance();
			e->d.TemplateSimpleSystem();
			break;
		case ID_FILE_NEWBINARYCIRCULARSYSTEM:
		{
			std::function<void(FPOINT, bool)> fT = [&](FPOINT distance, bool cw)
			{
				e->d.ResetDistance();
				e->d.TemplateBinaryCircularSystem(distance, cw);
			};
			::DialogBoxParam(hInst, MAKEINTRESOURCE(IDD_DLG_GETDISTANCE), hWnd, GetDistanceDlgProc, (LPARAM)&fT);
			break;
		}
		case ID_FILE_NEWBINARYELLIPTICALSYSTEM:
		{
			std::function<void(FPOINT, bool)> fT = [&](FPOINT distance, bool cw)
			{
				e->d.ResetDistance();
				e->d.TemplateBinaryElipticalSystem(distance, cw);
			};
			::DialogBoxParam(hInst, MAKEINTRESOURCE(IDD_DLG_GETDISTANCE), hWnd, GetDistanceDlgProc, (LPARAM)&fT);
			break;
		}
		case ID_FILE_NEWTHREEBODYSYSTEM:
		{
			std::function<void(FPOINT, bool)> fT = [&](FPOINT distance, bool cw)
			{
				e->d.ResetDistance();
				e->d.TemplateThreeBodySystem(distance, cw);
			};
			::DialogBoxParam(hInst, MAKEINTRESOURCE(IDD_DLG_GETDISTANCE), hWnd, GetDistanceDlgProc, (LPARAM)&fT);
			break;
		}
		case ID_BODIES_NEWBODY:
		{
			::DialogBoxParam(hInst, MAKEINTRESOURCE(IDD_NBODY_DIALOG), hWnd, NewBodyDlgProc, (LPARAM)&e->d);
			break;
		}
		case ID_BODIES_CLOCKWISE:
			cw = !cw;
			break;
		case ID_SIMULATION_GRAVITY:
			e->d.ToggleGravity();
			break;
		case ID_OPERATIONS_INTERACTIONS:
			e->d.ToggleGravitationalInteraction();
			break;
		case ID_OPERATIONS_TRAILS:
			e->d.ToggleTrail();
			break;
		case ID_VIEW_VELOCITY:
			e->d.ToggleVelocity();
			break;
		case ID_VIEW_DECREASEDISTANCE:
			f = []() { e->d.DecreaseDistance(); };
			break;
		case ID_VIEW_INCREASEDISTANCE:
			f = []() { e->d.IncreaseDistance(); };
			break;
		case ID_VIEW_RESETDISTANCE:
			e->d.ResetDistance();
			break;
		case ID_VIEW_CLEAR:
			e->d.ToggleClearCanvas();
			break;
		case ID_SIMULATION_INCREASESTEP:
			e->d.IncreaseStep();
			break;
		case ID_SIMULATION_DECREASESS:
			e->d.DecreaseStep();
			break;
		case ID_SIMULATION_CANCELVELOCITIES:
			e->d.KillVelocity();
			break;
		case ID_BODIES_MASSIVECOLLISION:
			e->d.DoMassiveCollision(e->d.GetMaxDistance(), 8);
			break;
		case ID_BODIES_CREATEMASSELESSBODY:
		case ID_BODIES_CREATEMASSIVEBODY:
			multQty = 500;
			f = [&]()
			{
				auto mode = (wmId == ID_BODIES_CREATEMASSELESSBODY ? 0 : 1);
				rcClient = lUpdateClientRect(hWnd, hWndStatusBar);
				POINT p1 = { rcClient.left, rcClient.top };
				POINT p2 = { rcClient.right, rcClient.bottom };
				auto lt = Vector();
				auto rb = Vector();
				e->v.GetCoordinateConverter().ConvertViewToWorld(p1, lt);
				e->v.GetCoordinateConverter().ConvertViewToWorld(p2, rb);
				e->d.CreateNewBody(mode, cw, lt, rb, true);
			};
			break;
		case ID_BODIES_DELETEBODY:
			multQty = 500;
			e->d.DeleteBody(multQty);
			break;
		case ID_BODIES_INCREASE10:
			f = []() { e->d.IncreaseFX(); };
			break;
		case ID_BODIES_DECREASE10:
			f = []() { e->d.DecreaseFX(); };
			break;
		case ID_BODIES_MULTIPLE10X:
			multiple = !multiple;
			break;
		case ID_SIMULATION_RUNNING:
			if(!e->d.IsPotentialMap())
				_running = !_running;
			break;
		case ID_VIEW_POTENTIALMAP:
		{
			static auto lastRunning = false;
			e->d.TogglePotentialMap();
			if (e->d.IsPotentialMap())
			{
				ProcessPotentialMap(e->p, e->d, e->v, hWnd, hWndStatusBar, _visible);
				lastRunning = _running;
				_running = false;
			}
			else
			{
				_running = lastRunning;
			}
			break;
		}
		default:
			return ::DefWindowProc(hWnd, message, wParam, lParam);
		}
		if (!e->d.IsPotentialMap())
		{
			if (f)
			{
				for (int i = 0; i < (multiple ? multQty : 1); i++)
				{
					f();
				}
			}
			e->d.SetUpdated(true);
			updateMenu();
			updateStatusBar(0.0f);
		}
		break;
	}
	case WM_GETMINMAXINFO:
	{
		auto info = reinterpret_cast<MINMAXINFO*>(lParam);
		info->ptMinTrackSize.x = _minWindowSize.cx;
		info->ptMinTrackSize.y = _minWindowSize.cy;
		break;
	}
	case WM_ACTIVATE:
	{
		_visible = !HIWORD(wParam);
		break;
	}
	case WM_LBUTTONDOWN:
	{
		auto tmassive = ((::GetKeyState(VK_SHIFT) & 0x8000) == 0x8000);
		auto tcw = ((::GetKeyState(VK_CONTROL) & 0x8000) == 0x8000);
		POINT p = { GET_X_LPARAM(lParam), GET_Y_LPARAM(lParam) };
		Vector pos;
		e->v.GetCoordinateConverter().ConvertViewToWorld(p, pos);
		e->d.CreateNewBody(pos, tmassive, tcw);
		e->d.SetUpdated(true);
		::InvalidateRect(hWnd, &rcClient, FALSE);
		break;
	}
	case WM_MOUSEWHEEL:
	{
		if (!e->d.IsPotentialMap())
		{
			if (GET_WHEEL_DELTA_WPARAM(wParam) > 0)
				e->d.DecreaseDistance();
			else
				e->d.IncreaseDistance();
			e->d.SetUpdated(true);
			if(!_running) ::InvalidateRect(hWnd, &rcClient, FALSE);
		}
		break;
	}
    case WM_PAINT:
	{
		if (e->v.OnPaint(hWnd, wParam, lParam) == 0)
		{
			return DefWindowProc(hWnd, message, wParam, lParam);
		}
		break;
	}
    case WM_DESTROY:
		e->v.OnDestroy(hWnd, wParam, lParam);
        ::PostQuitMessage(0);
        break;
	case WM_DISPLAYCHANGE:
		e->v.OnDisplayChangeHandler(hWnd, wParam, lParam);
		break;
	case WM_UPDATE_STATUSBAR:
	{
		updateStatusBar(1000.0f / (FPOINT)wParam);
		break;
	}

	case WM_CREATE:
	{
		auto c = reinterpret_cast<CREATESTRUCT*>(lParam);
		e = (Engine*)c->lpCreateParams;
		//Initializing engine
		e->v.OnCreate(hWnd, wParam, lParam);
		//Updating client window size information
		updateMenu();
		e->d.SetPotentialOffset(1);
		//Initializing window objects
		hWndStatusBar = lCreateStatusBar(hWnd);
		updateStatusBar(0.0f);
		menu = ::GetMenu(hWnd);
		break;
	}
	case WM_SIZE:
	{
		rcClient = lUpdateClientRect(hWnd, hWndStatusBar);
		::SendMessage(hWndStatusBar, WM_SIZE, 0, 0);
		updateMenu();
		//Updating canvas
		e->d.SetCanvas(rcClient);
		e->v.OnSize(hWnd, wParam, lParam);
		//Process potential map
		if (e->d.IsPotentialMap())
		{
			ProcessPotentialMap(e->p, e->d, e->v, hWnd, hWndStatusBar, _visible);
		}
		else
		{
			e->d.SetUpdated(true);
		}
		break;
	}
    default:
        return DefWindowProc(hWnd, message, wParam, lParam);
    }
    return 0;
}

static std::wstring lGetVersion(WCHAR* entry)
{
	std::wstring ret;

	HRSRC hVersion = ::FindResource(hInst, MAKEINTRESOURCE(VS_VERSION_INFO), RT_VERSION);
	if (hVersion != NULL)
	{
		HGLOBAL hGlobal = ::LoadResource(hInst, hVersion);
		if (hGlobal != NULL)
		{
			LPVOID versionInfo = ::LockResource(hGlobal);
			if (versionInfo != NULL)
			{
				DWORD vLen, langD;
				BOOL retVal;
				WCHAR* retbuf = NULL;
				static WCHAR fileEntry[256] = {};
				wsprintf(fileEntry, L"\\VarFileInfo\\Translation");
				retVal = ::VerQueryValue(versionInfo, fileEntry, (LPVOID*) &retbuf, (UINT *)&vLen);
				if (retVal && vLen == 4)
				{
					memcpy(&langD, retbuf, 4);
					wsprintf(fileEntry, L"\\StringFileInfo\\%02X%02X%02X%02X\\%s",
						(langD & 0xff00) >> 8, langD & 0xff, (langD & 0xff000000) >> 24,
						(langD & 0xff0000) >> 16, entry);
				}
				else
					wsprintf(fileEntry, L"\\StringFileInfo\\%04X04B0\\%s",
						::GetUserDefaultLangID(), entry);

				if (::VerQueryValue(versionInfo, fileEntry, (LPVOID*)&retbuf, (UINT *)&vLen))
					ret = (WCHAR*)retbuf;
			}
		}
		::FreeResource(hGlobal);
	}

	return ret;
}

// Message handler for about box.
INT_PTR CALLBACK AboutDlgProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
    UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
	{
		auto version = lGetVersion(L"FileVersion");
		std::wstringstream wss;
		wss << L"n-Body Simularion " << version << L"\nAuthor: Leonardo Correa Rocha\nleonardo.correa.rocha@hotmail.com";
		::SetDlgItemTextW(hDlg, IDC_STATIC, wss.str().c_str());

		break;
	}
	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
		{
			::EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		break;
	case WM_NOTIFY:
	{
		LPNMHDR pnmh = (LPNMHDR)lParam;
		// If the notification came from the syslink control
		if (pnmh->idFrom == IDC_SYSLINK)
		{
			// NM_CLICK is the notification is normally used.
			// NM_RETURN is the notification needed for return keypress, otherwise the control is not keyboard accessible.
			if ((pnmh->code == NM_CLICK) || (pnmh->code == NM_RETURN))
			{
				// Recast lParam to an NMLINK item because it also contains NMHDR as part of its structure
				PNMLINK link = (PNMLINK)lParam;
				::ShellExecute(NULL, L"open", link->item.szUrl, NULL, NULL, SW_SHOWNORMAL);
			}
		}
		break;
	}
	}
    return (INT_PTR)FALSE;
}

FPOINT lConvertStringToFPOINT(const char* v)
{
	return ((v != NULL && strlen(v) > 0) ? std::stof(v) : 0.0f);
}

FPOINT lGetDialog_FPOINT(HWND hDlg, int d)
{
	std::vector<char> buffer;
	buffer.resize(::GetWindowTextLengthA(::GetDlgItem(hDlg, d)) + 1);
	::GetDlgItemTextA(hDlg, d, &buffer[0], (int)buffer.size());

	return lConvertStringToFPOINT(&buffer[0]);
}

void lGetNewBodyDialog_Pos_FPOINT(HWND hDlg, Vector& p)
{
	p = Vector(
		lGetDialog_FPOINT(hDlg, IDC_PX),
		lGetDialog_FPOINT(hDlg, IDC_PY));
}

void lGetNewBodyDialog_Vel_FPOINT(HWND hDlg, Vector& p)
{
	p = Vector(
		lGetDialog_FPOINT(hDlg, IDC_VX),
		lGetDialog_FPOINT(hDlg, IDC_VY));
}

FPOINT lGetNewBodyDialog_Mass_FPOINT(HWND hDlg)
{
	return lGetDialog_FPOINT(hDlg, IDC_MASS);
}

std::string lConvertFPOINTToString(FPOINT f)
{
	std::ostringstream oss;
	oss << std::setprecision(2) << std::setiosflags(4096) << std::setw(9) << std::setfill(' ') << f;

	return oss.str();
}

static void lUpdateDialog_Text(HWND hDlg, int d, FPOINT f)
{
	::SetDlgItemTextA(hDlg, d, lConvertFPOINTToString(f).c_str());
}

static void lUpdateNBodyDialog_Pos_Text(HWND hDlg, FPOINT x, FPOINT y, FPOINT z)
{
	lUpdateDialog_Text(hDlg, IDC_PX, x);
	lUpdateDialog_Text(hDlg, IDC_PY, y);
	lUpdateDialog_Text(hDlg, IDC_PZ, z);
}

static void lUpdateNBodyDialog_Vel_Text(HWND hDlg, FPOINT x, FPOINT y, FPOINT z)
{
	lUpdateDialog_Text(hDlg, IDC_VX, x);
	lUpdateDialog_Text(hDlg, IDC_VY, y);
	lUpdateDialog_Text(hDlg, IDC_VZ, z);
}

static void lUpdateNBodyDialog_Vel_Text(HWND hDlg, Vector& v)
{
	lUpdateNBodyDialog_Vel_Text(
		hDlg,
		v.x,
		v.y,
		0.0f);
}

static void lUpdateNBodyDialog_Mass_Text(HWND hDlg, FPOINT m)
{
	lUpdateDialog_Text(hDlg, IDC_MASS, m);
}

void lUpdate_NBodyDialog(HWND hDlg, bool cw)
{
	::CheckDlgButton(hDlg, IDC_CHECK_CW, (cw ? BST_CHECKED : BST_UNCHECKED));
}

INT_PTR CALLBACK NewBodyDlgProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	static Data* data = NULL;
	static bool cw = true;
	switch (message)
	{
	case WM_INITDIALOG:
	{
		data = (Data*)lParam;
		//Setting initial values
		lUpdateNBodyDialog_Pos_Text(hDlg, 0.0f, 0.0f, 0.0f);
		lUpdateNBodyDialog_Vel_Text(hDlg, 0.0f, 0.0f, 0.0f);
		lUpdateNBodyDialog_Mass_Text(hDlg, 0.0f);
		lUpdate_NBodyDialog(hDlg, cw);
		break;
	}
	case WM_COMMAND:
		switch (LOWORD(wParam))
		{
		case IDOK:
		{
			//Getting data
			Vector pos;
			lGetNewBodyDialog_Pos_FPOINT(hDlg, pos);
			Vector vel;
			lGetNewBodyDialog_Vel_FPOINT(hDlg, vel);
			FPOINT mass = lGetNewBodyDialog_Mass_FPOINT(hDlg);
			//Creating new body
			data->CreateNewBody(pos, vel, mass);
			::EndDialog(hDlg, LOWORD(wParam));
		}
			return (INT_PTR)TRUE;
			break;
		case IDCANCEL:
			::EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)FALSE;
			break;
		case IDC_BUTTON_POS:
		{
			auto center = data->GetCoM();
			lUpdateNBodyDialog_Pos_Text(
				hDlg,
				center.x,
				center.y,
				0.0f);
		}
		break;
		case IDC_BUTTON_VEL:
		{
			Vector p;
			lGetNewBodyDialog_Pos_FPOINT(hDlg, p);
			Body b;
			if (data->GetMostAttractiveBody(p, b))
			{
				Vector v;
				Calculator::CalculateOrbitalVelocity(v, b.pos, b.vel, p, b.mass, cw);
				lUpdateNBodyDialog_Vel_Text(hDlg, v);
			}
			else
			{
				lUpdateNBodyDialog_Vel_Text(hDlg, 0.0f, 0.0f, 0.0f);
			}
		}
			break;
		case IDC_BUTTON_MASS:
			lUpdateNBodyDialog_Mass_Text(hDlg, data->GetTotalMass());
			break;
		case IDC_CHECK_CW:
			cw = !cw;
			break;
		}
		break;
	}
	return (INT_PTR)FALSE;
}

INT_PTR CALLBACK GetDistanceDlgProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	static std::function<void(FPOINT, bool)>* operation;
	static bool cw = true;

	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		operation = (std::function<void(FPOINT, bool)>*)lParam;
		::CheckDlgButton(hDlg, IDC_GETDISTANCEDLG_CW, (cw ? BST_CHECKED : BST_UNCHECKED));
		lUpdateDialog_Text(hDlg, IDC_GETDISTANCEDLG_EDIT, 1000.0f);
		break;
	case WM_COMMAND:
		{
			switch (LOWORD(wParam))
			{
				case IDOK:
				{
					auto distance = lGetDialog_FPOINT(hDlg, IDC_GETDISTANCEDLG_EDIT);
					(*operation)(distance, cw);
					::EndDialog(hDlg, LOWORD(wParam));
					break;
					return TRUE;
				}
				case IDCANCEL:
					::EndDialog(hDlg, LOWORD(wParam));
					return FALSE;
					break;
				case IDC_GETDISTANCEDLG_CW:
					cw = !cw;
					break;
			}
		}
		break;
	}

	return (INT_PTR)FALSE;
}
