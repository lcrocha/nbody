#include "stdafx.h"
#include "Data.h"
#include <algorithm>
#include <iostream>
#include <ppl.h>
#include <DirectXMath.h>

Data::Data()
{
	ClearData();
}

Data::~Data()
{
}

void Data::ClearData()
{
	_collisions.clear();
	_bodies.clear();
	_coM = Vector::Zero();
	_center = Vector::Zero();
	_maxCenterDistance = 0.0f;
	_maxCoMDistance = 0.0f;
	_trailLength = 10;
	_totalMass = 0.0f;
	_potentialMap.Clear();
	_updated = false;
	_potential = false;
	_gravitationalInteraction = false;
	_trail = false;
	_velocity = false;
	_clearCanvas= true;
	_maxDistance = 0.0f;
	_potentialOffset = 1;
	_step = 1e-3f;
	_gravity = true;
}

void Data::PushBody(Body& b)
{
	_bodies.push_back(b);
}

void Data::TemplateSimpleSystem()
{
	ClearData();
	Body a;
	a.mass = 1E+9f;
	a.pos = Vector::Zero();
	a.vel = Vector::Zero();
	PushBody(a);
}

void Data::TemplateBinaryCircularSystem(FPOINT r, bool cw)
{
	ClearData();
	Body b;
	b.mass = 1E+9;
	b.pos = Vector(-r, 0.0f);
	Calculator::CalculateOrbitalVelocity(b.vel, Vector::Zero(), Vector::Zero(), b.pos, b.mass, cw);
	b.vel = Vector::Scale(b.vel, 0.5f);
	PushBody(b);
	b.pos = Vector::Scale(b.pos, -1.0f);
	b.vel = Vector::Scale(b.vel, -1.0f);
	PushBody(b);
}

void Data::TemplateBinaryElipticalSystem(FPOINT r, bool cw)
{
	ClearData();
	Body b;
	b.mass = 1E+9;
	b.pos = Vector(-r, 0.0f);
	Calculator::CalculateOrbitalVelocity(b.vel, Vector::Zero(), Vector::Zero(), b.pos, b.mass, cw);
	b.vel = Vector::Scale(b.vel, 0.25f);
	PushBody(b);
	b.pos = Vector::Scale(b.pos, -1.0f);
	b.vel = Vector::Scale(b.vel, -1.0f);
	PushBody(b);
}

void Data::TemplateThreeBodySystem(FPOINT r, bool cw)
{
	ClearData();
	Body a;
	a.mass = r;
	a.pos = Vector::Scale(Vector(0.9700436f, -0.24308753f), a.mass);
	a.vel = Vector::Scale(Vector(0.466203685f, 0.43236573f), (cw ? -1.0f : 1.0f));
	PushBody(a);

	Body b;
	b.mass = a.mass;
	b.pos = Vector::Scale(a.pos, -1.0f);
	b.vel = a.vel;
	PushBody(b);

	Body c;
	c.mass = a.mass;
	c.pos = Vector::Zero();
	c.vel = Vector::Scale(a.vel, -2.0f);
	PushBody(c);
}

bool Data::Initialize()
{
	TemplateSimpleSystem();

	return false;
}

bool Data::Finalize()
{
	_bodies.clear();

	return false;
}

void Data::LockBodies(BodiesForLock& bb)
{
	bb.resize(_bodies.size());
	std::copy(_bodies.begin(), _bodies.end(), bb.begin());
}

void Data::UnlockBodies(BodiesForLock& bb, bool update)
{
	if (update)
	{
		std::copy(bb.begin(), bb.end(), _bodies.begin());
	}
}

bool Data::BrowseBodies(BrowseBodiesFunction f, bool deleteBodies)
{
	if (deleteBodies)
	{
		auto size = _bodies.size();
		Bodies bodiesOut;
		concurrency::parallel_for_each(_bodies.begin(), _bodies.end(), [=, &bodiesOut](auto& b)
		{
			if (!b.IsDeleted())
			{
				f(b);
				bodiesOut.push_back(b);
			}
		});
		_bodies.swap(bodiesOut);
	}
	else
	{
		concurrency::parallel_for_each(_bodies.begin(), _bodies.end(), [=](auto& b)
		{
			if (!b.IsDeleted())
			{
				f(b);
			}
		});
	}

	return false;
}

bool Data::InteractBodies(InteractBodiesFunction f)
{
	auto size = (int)_bodies.size();
	concurrency::parallel_for(0, size - 1, [&](auto i)
	{
		auto& b1 = _bodies[i];
		if (!b1.IsDeleted())
		{
			concurrency::parallel_for(i + 1, size, [&](auto j)
			{
				auto& b2 = _bodies[j];
				if (!b2.IsDeleted())
				{
					f(b1, b2);
				}
			});
		}
	});

	return false;
}

bool Data::GetGreaterMassBody(std::function<void(Body&)> f)
{
	concurrency::combinable<Body*> combBody;
	BrowseBodies([&](Body& b)
	{
		auto& local = combBody.local();
		local = (local != nullptr && local->mass > b.mass ? local : &b);
	});
	auto b = combBody.combine([](Body* b1, Body* b2)
	{
		return (b1->mass > b2->mass ? b1 : b2);
	});

	if(nullptr != b)
		f(*b);

	return (b != nullptr);
}

void Data::KillVelocity()
{
	BrowseBodies([](Body& b)
	{
		b.vel = Vector::Zero();
	});
}

void Data::IncreaseFX()
{
	BrowseBodies([](Body& b)
	{
		b.mass *= 1.1f;
	});
}

void Data::DecreaseFX()
{
	BrowseBodies([](Body& b)
	{
		b.mass *= 0.9f;
	});
}

bool Data::GetMostAttractiveBody(Vector& pos, Body& b)
{
	Body nb;
	nb.mass = 0.0f;
	nb.pos = pos;

	concurrency::combinable<Body*> combBody;
	BrowseBodies([&](Body& b)
	{
		if (b.mass > 0.0f)
		{
			auto& local = combBody.local();
			if (local == nullptr)
			{
				local = &b;
			}
			else
			{
				auto f1 = Vector::Length(Calculator::GetForce(nb.pos, nb.mass, b.pos, b.mass));
				auto f2 = Vector::Length(Calculator::GetForce(nb.pos, nb.mass, local->pos, local->mass));
				if (f1 > f2)
				{
					local = &b;
				}
				else if (f1 == f2)
				{
					auto bDist = Vector::Length(Vector::Sub(nb.pos, b.pos));
					auto localDist = Vector::Length(Vector::Sub(nb.pos, local->pos));
					if (bDist < localDist)
					{
						local = &b;
					}
				}
			}
		}
	});
	auto pb = combBody.combine([&nb](Body* b1, Body* b2)
	{
		auto f1 = Vector::Length(Calculator::GetForce(nb.pos, nb.mass, b1->pos, b1->mass));
		auto f2 = Vector::Length(Calculator::GetForce(nb.pos, nb.mass, b2->pos, b2->mass));
		return (f1 > f2 ? b1 : b2);
	});
	if (pb != nullptr)
	{
		b = *pb;
	}

	return (pb != nullptr);
}

void Data::CreateNewBody(Vector& pos, FPOINT m, bool cw)
{
	Body nb, b;
	nb.pos = pos;
	nb.mass = m;
	if (GetMostAttractiveBody(pos, b))
	{
		Calculator::CalculateOrbitalVelocity(nb.vel, b.pos, b.vel, nb.pos, b.mass, cw);
	}
	PushBody(nb);
}

void Data::CreateNewBody(int mode, bool cw, Vector& lt, Vector& rb, bool orbitalVelocity)
{
	auto width = rb.x - lt.x;
	auto height = rb.y - lt.y;
	Body c;
	Body nb;
	nb.pos = Vector(lt.x + width * RNG::Get().RandFPOINT(), lt.y + height * RNG::Get().RandFPOINT());
	nb.mass = (mode == 0 ? 0.0f : GetMassiveBodyMass());
	if (GetMostAttractiveBody(nb.pos, c))
	{
		Calculator::CalculateOrbitalVelocity(nb.vel, c.pos, c.vel, nb.pos, c.mass, cw);
	}
	PushBody(nb);
}

FPOINT Data::GetMassiveBodyMass()
{
	auto factor = RNG::Get().RandFPOINT();

	return MAX(1E-6f*_totalMass, 1E+6f)*(0.8f*factor + 0.2f);
}

void Data::CreateNewBody(Vector& pos, bool massive, bool cw)
{
	CreateNewBody(pos, (massive ? GetMassiveBodyMass() : 0.0f), cw);
}

void Data::CreateNewBody(Vector& p, Vector& v, FPOINT m)
{
	Body nb;
	nb.pos = p;
	nb.vel = v;
	nb.mass = m;
	PushBody(nb);
}

void Data::DeleteBody(unsigned int qty)
{
	auto size = _bodies.size();
	if (size > 0)
	{
		if (size > qty)
		{
			_bodies.resize(size - qty);
		}
		else
		{
			_bodies.clear();
		}
	}
}

unsigned Data::GetTotalBodies()
{
	return (unsigned)_bodies.size();
}

unsigned Data::CollisionMethodTotal()
{
	return (unsigned)_collisions.size();
}

unsigned Data::CollisionMethod(unsigned c)
{
	unsigned ret = 0;
	if (c < CollisionMethodTotal())
	{
		ret = _collisions[c];
	}

	return ret;
}

void Data::CollisionMethodInc(unsigned c)
{
	while (c >= CollisionMethodTotal())
	{
		_collisions.push_back(0);
	}
	_collisions[c]++;
}

void Data::DoMassiveCollision(FPOINT distance, int qty)
{
	auto centerX = _coM.x;
	auto centerY = _coM.y;
	Body b;
	b.mass = (_totalMass > 0.0f ? 0.01f*_totalMass : 1e6f);
	for (int i = 0; i < qty; i++)
	{
		auto angle = i*DirectX::XM_2PI / qty;
		auto np = Vector(
			distance*::cosf(angle),
			distance*::sinf(angle));
		b.pos = Vector::Add(_coM, np);
		b.vel = Vector::Scale(Vector::Normalize(np), -0.01f*distance);
		PushBody(b);
	}
}