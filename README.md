n-Body simulation  
tags: C++11, DirectX, Math, PPL, Parallelism, Physics, GDI, Direct2D, DirectWrite, Kepler laws, Newton laws, Gravitation  

-> This is my n-body simulation implementing Newton and Kepler laws using C++11.  

Commads:  
n-Body Simularion  
author: Leonardo Correa Rocha  
leonardo.correa.rocha@hotmail.com  
Help:  
View:  
a, A:			Toggle Cancel/Not Cancel Gravity  
g, G:			Toggle Show/Hidden Gravitional Interaction lines  
t, T:			Toggle Show/Hidden body trail  
------  
+:		[1]	Decrease View distance  
-:		[1]   	Increase View distance  
Backspace:		Reset View distance  
c, C:			Toggle Clear/Not Clear canvas  

Simulation:  
Left:		[1]   	Decrease animation Step  
Right:		[1]   	Increase animation Step  
k, K:			Toggle Cancel/Not Cancel Velocities  
			Not pressed:	CounterClockwise  
Operations:  
8:			Create a two massive bodies toward the center  
9:		[1]   	Increase 10% mass to all  
0:		[1]   	Decrease 10% mass to all  
n:		[1,2] 	Create masseless body  
m:		[1,2] 	Create massive body  
d:		[1]	Delete body  

Modificators for operations:  
1 - Shift:		Pressed: 	10 operations  
			Not pressed: 	1 operation  
2 - Control:		Pressed:	Clockwise  